---
title: Git Service - Service Level Description
---

## Summary

This service offers hosting of code repositories for the Git distributed version control system.

Git is a distributed version control system, and offers an alternative to SVN for software code hosting.

## Homepage

The hosting platform for Git repositories is currently located at <https://gitlab.cern.ch>.

## Service Managers

Ismael Posada Trobo (ismael.posada.trobo@cern.ch)

IT Department - PW-WA section

## General Description

### Supported Clients

Support is provided for the Git command-line client available on lxplus and current CERN Linux distributions (via the git​ package). GitLab also allows for file edits and upload via a modern web browser.

A version of Git for Windows is available from CMF. It is provided as-is.

While most developer tools have integrated support for Git natively nowadays and will usually work, their behavior when used with the Git service cannot be guaranteed. Some features like Kerberos authentication may not be available in all such tools.

:::note

Operating the client software is the responsibility of the user when not using the standard command line client as provided on the lxplus service and CERN Linux distributions.

:::

### User Responsibilities

Git is intended for text and source code. Binaries, JAR files, pictures, etc. would fit better in other services provided by the CERN IT Department. Please check this [KB article](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003887) to find a set of recommendations on how to use Git and alternatives in case what you need is an storage system.

### Git repository hosting platforms

The Git service is currently running GitLab, a modern platform introduced in spring 2015. In addition to repository hosting, GitLab offers GitHub-like code review and collaborative features (in particular a merge requests workflow). This is a fully self-service platform and is the recommended place for all new projects. Please read [our guide to get started with GitLab](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137). GitLab also offers:

- A built-in Continuous Integration engine to facilitate automatic test, build and deployment of software projects. See [our article about GitLab-CI](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003690).
- A built-in Docker container and package registry available in **gitlab-registry.cern.ch** Detailed information is provided in [this article](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0004284).
- Owners of SVN repositories are encouraged to move them to GitLab. See our article on [migrating projects from SVN](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0001905).

### Data backups

Data backups can be classified in two:

- GitLab CephFS backups (git data).
- Non-git data backups.

#### GitLab CephFS Backups

Gitlab data is currently stored on 4 CephFS shares provided by the Ceph Service. Backup of this areas is performed by the Ceph Team (IT-SD):

```bash
/cephfs-levinson/volumes/_nogroup/2df13044-180f-43b0-9d17-7361c1857448/03051040-19ee-4c81-9856-eeeaf95f2841
/cephfs-levinson/volumes/_nogroup/44093fe3-a050-4573-9f34-4d50f1e14066/5ef27bba-7b37-4e02-8e1d-9966eac961c2
/cephfs-levinson/volumes/_nogroup/c47fa02b-277c-4086-9357-868a65f696e4/da0e9066-5620-441f-8fa7-5ee3ef8da23d
/cephfs-levinson/volumes/_nogroup/82517cc4-ccdf-4ab9-b473-ca41e416284e/e9e9ce96-fca4-4b39-9608-9b6dd31d6375
```

##### Backup frequency and retention policy

- All four shares are backed-up twice per day, at ~07:00 and ~19:00.
- A retention policy (purge of old backups) is applied like the following:
  - Daily backups up to two months back (2 daily backups for the last month).
  - Weekly backups up to 6 months back.
  - Monthly backups up to 12 months back.

![Backup frequency and retention policy](/img/assets/sld/cephfs-backup-retention-policy.png)

##### Backup features

- Backup data is stored in a different geographical location (Prevessin, FR) than the source data.
- Backup data is encrypted using AES-256.

##### Backup consistency

- Each cephfs volume is backed-up independently. A time skew between backups of each volume is to be expected.
- Backup is performed from a live file system, hence point-in-time consistency cannot be guaranteed.
- The GitLab team is responsible for regularly exercising and verifying that backups are usable.

#### Non-git data

Non-git data (Docker images, user uploads, job artifacts) is stored in [S3](https://cern.service-now.com/service-portal?id=functional_element&name=S3ObjectStorage). S3 versioning is configured so that deleted items are kept for 1 month before being removed from storage. In addition to S3's own internal redundancy and data protection mechanisms, a replica of the GitLab content stored in S3 is kept on a separate storage system (Ceph RBD volumes) and updated daily.

## Service Documentation

Documentation is available under <https://gitlab.docs.cern.ch>.

- GitLab-specific documentation is under the [GitLab topic](https://cern.service-now.com/service-portal?id=cern_kb_search&spa=1&query=Gitlab) (in particular our [Getting started](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137) guide) and in [GitLab's help page](https://gitlab.cern.ch/help).
- General help about Git usage and best practices can be found in the "Further guides" section on this page and under the topic ["Using Git"](https://cern.service-now.com/service-portal/?id=cern_kb_search&spa=1&query=Using%20Git).

## Service Level

- Use of the Git service is granted to **CERN account holders** inside and outside CERN. Users without a CERN account cannot login to gitlab.cern.ch. Public projects with contributors from the general public or projects requiring collaboration with persons or companies not entitled to a CERN account may consider public Git repository hosting platforms such as GitHub or gitlab.com. For more details see [KB0003132](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003132).
- The Git service aims at providing version control for **​text and source code**​. Storage of large binary files in general is outside the scope of the service. As a number of [options exist already at CERN](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003887), Git-LFS ([Large File Storage](https://gitlab.cern.ch/help/topics/git/index.md#git-large-file-storage-lfs)) is only provided with a restrictive quota (10GB per GitLab project as of May 2018) to help with migration from SVN.
- Service support is via the Service Portal and service availability is indicated on the availability displays available from the portal or this page.
- Like most other IT applications, Git and Gitlab service hours are regular CERN working hours, e.g. weekdays from 8:30 to 17:30. Although the services are designed to be reliable and downtime is not expected, any service releated intervention outside these hours are only on a "best effort" basis.
- GitLab Projects hosted in a user's personal workspace are archived after the user leaves CERN. GitLab Groups are however persistent and ownership can be transferred to other persons as necessary. Guidelines can be found in the ["Getting Started" article](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0003137).
- Any project or users that are found to abuse the service or not respecting the [CERN Computing rules](https://security.web.cern.ch/security/rules/), may be removed upon notification of the administrators of that repository.

## Feedback

To send us your suggestion, or to make a feature request please go to the [Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
