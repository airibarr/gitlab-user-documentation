---
title: Contribute to Gitlab
---

GitLab is an open source software development platform powered by it's enormous community that finds, fixes and reports issues and bugs. More than 4,000 users are involved in the development of GitLab and contribute vastly to the open source code.

At CERN, we get a lot of tickets about upstream issues that users identify which we can report to GitLab after checking if there is not an issue about it already.

### Did you found a bug?

If something is not behaving as expected or GitLab shows error messages that it shouldn't, probably you found a bug.

### Check if the issue is already reported

Try to describe the issue in simple words and see if other users have reported the same issue on [GitLab's issues](https://gitlab.com/groups/gitlab-org/-/issues/). If you found the issue, you can report it to us by [submitting a ticket](https://cern.service-now.com/service-portal?id=service_element&name=git-service). Press the thumbsup button (👍) in the issue or leave a comment to let them know more users are facing the same issue and you want to be notified in the event of a comment/action in the issue.

### Open a new issue upstream

If you didn't found a similar issue, take the initiative and report it first following the steps below:

1. If you don't have already an account create sign up here [gitlab.com](https://gitlab.com/users/sign_up)
2. Create a new issue by clicking on [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new)
3. Fill in the following fields:
    1. **Title**. Make the title concise and informative
    2. **Type**. It is set to `Issue` already
    3. **Description**. Choose the suitable template or search for the `Bug` template
4. Fill the information in the template as it is described. For example the bug template requires the following:
    1. **Summary**. Summarize the bug encountered concisely
    2. **Example Project**. If possible, create an example project here on GitLab.com that exhibits the problematic behavior
    3. **Relevant logs and/or screenshots**. Paste logs you found if applicable.
    4. **Possible fixes**. If you can, link to the line of code that might be responsible for the problem
5. Do not forget to inform us by [submitting a ticket](https://cern.service-now.com/service-portal?id=service_element&name=git-service), referencing the issue you have created.

In case to provide the current version we are running at CERN is needed, you can get from the header of the [GitLab Mattermost channel](https://mattermost.web.cern.ch/it-dep/channels/gitlab), or go to <https://gitlab.cern.ch/help/> and see the version in the top of the page.

### More info about contributing to GitLab

For more information about how to contribute see [GitLab's contribution documentation](https://docs.gitlab.com/ee/development/)
