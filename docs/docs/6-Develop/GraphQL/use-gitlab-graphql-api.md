# Using the GitLab GraphQL API

The GraphQL API for GitLab provides a way to interact with GitLab's functionality programmatically using GraphQL [queries](https://docs.gitlab.com/ee/api/graphql/getting_started.html#queries-and-mutations) and [mutations](https://docs.gitlab.com/ee/api/graphql/getting_started.html#mutations). With the GraphQL API, you can retrieve information about projects, issues, merge requests, users, and other resources within GitLab.

:::warning

Be aware when executing `mutations`. A mutation is a change (update) action, therefore it can have unexpected consequences if not properly used.

:::

To interact with the GraphQL API for GitLab, you need to authenticate with a personal access token or an OAuth2 token. The [API documentation](https://docs.gitlab.com/ee/api/graphql/reference/) provides detailed information about available queries, mutations, and their respective input and output types. Refer to the official documentation about the [GraphQL API](https://docs.gitlab.com/ee/api/graphql/) for further information.

:::tip

You can explore the GraphQL schema, experiment with queries, and test API calls using tools like GraphQL Playground or GraphiQL, provided by GitLab under <https://gitlab.cern.ch/-/graphql-explorer>

:::

## Use case: fetch all projects that match a pattern

An example of using the GraphQL API is to fetch all projects that match a given pattern. An user can do:

```json
query{ 
  groups(search: "atlas-") {
    edges {
      node {
        id
        fullPath
      }
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
```

This will result in the following output:

```json
{
  "data": {
    "groups": {
      "edges": [
        {
          "node": {
            "fullPath": "atlas-phys-exotics-llp-mscrid/36fbanalysis"
          }
        },
        {
          "node": {
            "fullPath": "atlas-lar-be-firmware/ABBA"
          }
        },
        {
          "node": {
            "fullPath": "atlas-itp-dcs"
          }
        },
        ...
      ],
      "pageInfo": {
        "endCursor": "eyJxxx",
        "hasNextPage": true
      }
    }
  }
}
```

In this result, we can observe the value of `pageInfo.endCursor`. This is useful for pagination. See the following example to continue fetching above projects from this `endCursor`:

```json
query{ 
  groups(search: "atlas-", after: "eyJxxx") {
    edges {
      node {
        id
        fullPath
      }
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
```

## Use `curl` against the GraphQL API

For more automated tasks, the graphql-explorer can be a bit limited. Therefore, a user can use `curl` together with the speficic payload for the GraphQL API. Refer to <https://docs.gitlab.com/ee/api/graphql/getting_started.html#command-line> for further information.

## Combine both GraphQL and GitLab API usages

While both entities can be treated separately and majority of actions can be executed on one or the other, it can be very useful to combine both GraphQL's output to be used as GitLab API's input.

For example, assuming the above example, next example action will be to set specific owners for a given project (with its `id`) through the GitLab API.

So the `curl` command will end up like:

```bash
PRIVATE_TOKEN="xxx"
USER_ID="123"
ACCESS_LEVEL="30"

curl --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
     --data "user_id=${USER_ID}&access_level=${ACCESS_LEVEL}" "https://gitlab.cern.ch/api/v4/projects/<:id>/members"
```

n.b.: access levels can be any of the following [roles](https://docs.gitlab.com/ee/user/permissions.html#roles): `10` (Guest), `20` (Reporter), `30` (Developer), `40` (Maintainer), and `50` (Owner). Defaults to `40`. See further information under <https://docs.gitlab.com/ee/user/project/members/#which-roles-you-can-assign>
