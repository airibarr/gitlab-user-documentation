---
title: Getting started with GitLab
slug: /
---

## Overview

GitLab is a self-service code hosting application based on Git that provides collaboration and code review features (similar to GitHub). To access the application, go to <https://gitlab.cern.ch>.

:::info

For all GitLab features, it is necessary to visit <https://gitlab.cern.ch> in a web browser at least once with each account that needs access to GitLab, in order to initialize this account.

:::

Please note that by default your Gitlab account profile will be **public**, and therefore accessible from Internet. If you want to protect your user profile, please follow [KB0006803](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006803).

## Appropriate usage of GitLab

Sometimes users wonder what code hosting application is more appropriate for their use case. Below you will find some hints about the proper way of identifying what to use.

### When using CERN GitLab is more appropriate

CERN GitLab is appropriate for the following use cases:

- If your code contains sensitive information like configuration details, or anything that needs to be protected... (Passwords and other secrets should not be in the code, but stored with [tbag](http://configdocs.web.cern.ch/configdocs/secrets/adding.html) or another secure mechanism.)
- If your code is to be used in a project or service at CERN, and is vital for the functioning of equipment or services, accelerators or experiments at CERN. In particular, this is mandatory in case your code should be deployed on computers without access to the Internet as only the CERN git service provides an trusted internal CERN source;
- If your code needs to be restricted in access (using e-groups) to a limited audience at CERN or to CERN collaborators (i.e., holders of a CERN computing account). See further information about [access to GitLab at CERN](./1-User%20account%20options/account-access.md).
- If you need tight integration with other CERN IT services, such as SSO (Single Sign On), e-groups, JIRA, GitLab Pages, etc.

### When using GitHub, GitLab.com, BitBucket (or similar public services) is more appropriate

- If none of the above applies.
- If your code is supposed to be world-wide visible ("Open Source") and/or to be shared with others external to CERN. This is in particular interesting for your CV.
- If you have a shared project with external contributors.

When using external repositories:

- For CERN projects, it is essential to indicate that CERN is the copyright owner. Please use the appropriate [CERN area on Github](https://github.com/cern) and/or to include the [CERN logo](http://design-guidelines.web.cern.ch/badge-logo) and [licencing conditions](http://legal.web.cern.ch/licensing/software-terms-use).
- **Please make sure to [mirror (Pull method)](https://docs.gitlab.com/ee/user/project/repository/mirror/#repository-mirroring "mirror (Pull method)") the external repository into CERN GitLab to have a backup copy in case of failure of the external repository provider, and make it possible to locate and reference externally hosted CERN software projects.**
- If the project requires CI beyond the capacity available for free on the public services, as of May 2020 GitLab.com is recommended rather than GitHub.com. This is because of security issues when [adding custom runners to GitHub.com public projects](https://help.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#self-hosted-runner-security-with-public-repositories "it is not safe to add custom runners to GitHub.com public projects"). On the other hand, private runners can be connected to projects on GitLab.com just like for CERN GitLab.

## Concepts

GitLab introduces two important concepts:

- **Separate Personal and Group workspaces**: Git repositories are organized in a hierarchy, with top-level workspaces containing one or more projects (each project corresponds to a Git repository). Every user is granted a Personal workspace that can be used to host personal projects, tests or as a temporary workspace to work on a fork of a team project; like home directories, _the Personal workspaces will be deleted as users leave CERN_. On the other hand, Group workspaces are meant to host persistent, long-term projects and enable teams to work together. Group workspaces can be mapped to e-groups (known in GitLab as "LDAP groups"). See also [the help page about GitLab Groups](https://gitlab.cern.ch/help/user/group/index.md).
- **Merge Requests and code review**: instead of providing fine-grained permissions, GitLab encourages a review process for modifications. Developers who are not trusted with full permissions on a given repository write new code in separate feature branches or forks of the main project, then submit the modifications via a Merge Request. Developers with full permissions can review and comment the modifications before accepting (or not) to merge the changes. For details about GitLab's review workflow, please refer to the [workflow help pages](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/) and [KB0003135](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003135).

## Start using Git

First, make sure Git is configured with the correct email address. For GitLab to properly identify you as the author of a commit, it is important to configure git on your machine to use the same email address as your [account's main email address](https://users-portal.web.cern.ch/) (also visible in your [GitLab profile](https://gitlab.cern.ch/profile)):

```bash
git config --global user.email "your.primary@email.address".
```

More information about the usage of Git from the command line can be found in [this GitLab article](https://gitlab.cern.ch/help/gitlab-basics/start-using-git.md).

GitLab's help pages provides also a very useful [information about git concepts and basics](https://gitlab.cern.ch/help/topics/git/index.md).

## Create a Group workspace in GitLab

For creating a Group workspace in GitLab, refer to the [Group Management](./2-Groups/manage-groups.md) document.

## Create or import a project/repository

A GitLab project corresponds to a Git repository. To create a project:

- From [GitLab's dashboard](https://gitlab.cern.ch/), click the ['New Project'](https://gitlab.cern.ch/projects/new) button.
- In "project path", enter the desired repository name.
- Select your Group in the "Namespace" (the Personal workspace must be used only for tests and personal projects, as it will be treated like a home directory and deleted as users leave CERN).
- While there is an option to import from an existing repository, it is not recommended to use it at this point. Please see below for options to import an existing repository. the 'Internal' visibility level will grant read access to all authenticated CERN users; the 'Public' visibility level will allow any Internet anonymous user (including search engines) to browse and clone the repository. More information about visibility levels and sharing projects in [KB0003122](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003122).

To import an existing repository from SVN please see [KB0001905](https://cern.service-now.com/service-portal?id=kb_article&n=KB0001905).

## Help and support information

Please read the [Service Level for the Git service](99-service-level-description.md) for information about scope, limits and support hours for GitLab.

GitLab main help articles can be found in this documentation.
