# Project and group visibility

## How do I publish my GitLab project to CERN/external users?

In GitLab, a project (i.e. a repository) can be configured with 3 levels of visibility:

- **Private** (the default): only authenticated and explicitly authorized CERN users can access the project (users can be authorized at the level of the project or its parent Group). The project is hidden from anonymous users. Details about project/group permission can be found in GitLab's help pages. Our Getting Started with GitLab article covers usage of e-groups for permissions.
- **Internal**: all CERN users get read-only access to the project, including code history, and may clone and fork the project. It does not matter whether the CERN user is connecting from the CERN network or a remote location, but authentication is mandatory to access the project. The project is hidden from anonymous users.
- **Public**: anonymous users from both Internet and the CERN network get read-only acces to the project, including code history and may clone the project. The project is fully public on the Internet in a way similar to code hosting services such as GitHub, Bitbucket or Google Code. Anonymous users can search for and browse Public projects via GitLab's Explore page.
For more details and instructions on how to set this visibility level, see GitLab's help pages.

In addition to the global project visibility, _the visibility of several components of the project can be controlled individually in the project settings_. For instance, anonymous users can only clone a "Public" project if the sub-setting for repository visibility is "Everyone with access"; this enables configurations where the repository cannot be browsed or cloned by anonymous users while the project description and issue tracker remain public, for instance. When changing a project visibility, always review the individual component visibility as well.

The visibility of a GitLab project cannot be less restrictive than the visibility of the Group that contains it (i.e. a Private Group cannot contain Public projects).

Notice that GitLab does not offer a "CERN Public" setting that would allow a project to be accessible anonymously, but on the CERN network only: Public projects are always visible to anonymous Internet users as well. See other KB articles for how to enable automatic build and deployment systems to access GitLab projects without making the project Public.

The same policies applied for a GitLab project, apply to the Docker image pushed to the GitLab registry. More information about the GitLab registry can be found in [Packages and Registries](../4-Build%20your%20application/index.md).

:::danger

Please, be mindful of the consequences of setting a project's visibility to "Public". The GitLab service (<https://gitlab.cern.ch>) is open to Internet access and **not restricted to the scope of the CERN network**. A Public repository would be fully accessible from the Internet without any authorization mechanism, and would for instance be visited by search engines and robots. Any repository containing sensitive software or information must never be configured with "Public" visibility.

This is specially sensitive for **Pipelines Visibility**. Once a repository is set to "Public", pipeline visibility becomes "Public" by default, and any misuse of environment variables and/or secrets, may be printed unintentionally in the [job logs](https://docs.gitlab.com/ee/administration/job_logs.html), leaving them visible from any anonymous user. Check <https://docs.gitlab.com/ee/ci/pipelines/settings.html#change-pipeline-visibility-for-non-project-members-in-public-projects> before making your repository "Public".

Further information about Security best practices for your repository(ies) can be found under [Secure your application](../5-Secure%20your%20application/index.md).

:::

If you are unsure whether your project could be made "Public" or should instead be kept "Private" or "Internal", please consider contacting the [IT Computer Security Team](https://cern.service-now.com/service-portal?id=sc_cat_item&name=computer-security&se=computer-security) for advice on your specific case before taking any decision.

For more information about what accounts can access/collaborate to your project, please refer to [Access to GitLab at CERN](../1-User%20account%20options/account-access.md) document.

Last but not least, find more information about project and group visibility under the official documentation <https://docs.gitlab.com/ee/user/public_access.html>.
