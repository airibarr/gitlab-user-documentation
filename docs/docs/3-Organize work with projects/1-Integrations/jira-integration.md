---
title: GitLab and Jira integration
---

## Overview

GitLab projects and groups can be integrated with [Jira](https://cern.service-now.com/service-portal?id=service_element&name=JIRA-service) for Issue Tracking (bug tracking, task assignment, agile workflows etc.) but a few extra steps are required to activate the Jira integration features.

## Enabling the Jira integration

In order to enable the Jira Integration, it is required to have owner permission for the project or group you want to integrate.

If you have multiple projects under a single group that you would like to integrate with Jira, there is the possibility to enable the integration for the group directly. Any projects within the group will inherit the integration as well.

Follow these steps to enable Jira integration with your project or group:

1. Go to your project's/group's `Settings` menu option and click on `Integrations`.
![Location of integration settings](/img/assets/jira-integration/setting-menu.png)

2. From the list find `Jira` integrations
![Integrations List](/img/assets/jira-integration/jira-integration.png)

3. Enable integration by first clicking on `Use custom settings` and after that click on `active` check button on the top of the page.

  :::important

  Do not modify any of the `Connection details` field to inherit the global settings such as URL and personal access token.

  :::

  ![Enable Jira](/img/assets/jira-integration/jira-configuration.png)

4. Finally, Save the changes and you are ready!

![Save changes](/img/assets/jira-integration/save-changes.png)

After the integration is active, the default issue tracker for the GitLab project/group will be <https://its.cern.ch/jira> and any mention of a Jira issue (such as PROJECT-123) in a commit message, will link to the corresponding issue page in the central instance of the Jira issue tracker <https://its.cern.ch/jira>.

With additional configuration in your Jira project, it is possible to create backlinks from the Jira issue to the commits that mentioned the issue, and to automatically close a Jira issue when the comment message contains text such as "Fix PROJECT-123". It is also possible to enforce that every commit pushed to GitLab must mention a Jira issue.

## Enabling links from a Jira issue to commits that mention it

GitLab can link commit into a Jira issue whenever it is mentioned in a commit message, so as to create a backlink from the Jira issue to the commit that mentioned it.

In order to enable this, you need to grant the "Gitlab service" account the appropriate role that will give the following permissions "Browser project", "Add comments" and "Edit Links" (If you use your own service account, you need to specify that one instead.) Generally speaking, when using the global Permission schema "Default Permission Schema" these permissions are granted to Users role. But for different Permission schemas better to follow the next procedure.

1. Navigate to your Jira project's permissions page: `https://its.cern.ch/jira/plugins/servlet/project-config/project_key/permissions` where project_key is the key of your Jira project.
2. Locate the "Add Comments", "Browser Project" And "Link issues" permissions. The right column should list one or more project roles that are granted each permission, for instance "Project Role (Users)". Most projects will have the "Users" role here, but some projects might be using another role.
3. Navigate to your Jira project's roles page: `https://its.cern.ch/jira/plugins/servlet/project-config/project_key/roles` where project_key is the key of your Jira project.
4. Edit the "Users" column for the Role identified in step 2 to be allowed to "Add Comments" (in most cases, this will be the Users role) and add user "GitLab service - gitlab.service@cern.ch (gitlab)"

Every new commit in any GitLab project mentioning an issue in this Jira project will then trigger a comment in the Jira issue, providing a link to the commit.

## Enabling closing a Jira issue with a commit message

GitLab can automatically close Jira issues whenever a merge request or commit message containing text such as "Closes PROJECT-123" or "Fixes PROJECT-123" is merged into the project's default branch. See [GitLab's dedicated help page](https://gitlab.cern.ch/help/user/project/issues/managing_issues.html#closing-issues-automatically) for details about the exact behavior.

In order to enable this, two actions need to be performed. Firstly, enable Jira transitions in the GitLab UI with one of two options, and secondly, grant the "GitLab Service" account permission to "Close issues" in your Jira project.

- **Enable Jira transitions in GitLab**: Once your integration is active, these are the steps for enabling the automating issue closing of Jira issues:
    1. Navigate to your group's/project's `Settings` -> `Integrations` then click on `Jira` from the list of integrations.
    2. Enable the option "Enable Jira transitions" under the "Transition Jira issues to their final state" section.
    3. There are two configuration options for enabling the Jira transitions:
        - Move to `Done`
            - Using the "Move to Done" option will make GitLab attempt to find a correct transition for closing your Jira tickets. Check the option and press "Save changes" to enable it. You do not need to follow any extra steps, but results are not guaranteed as they highly depend on your Jira workflow.
        - Use custom transitions
            - If "Move to Done" does not work for your integration, then it is necessary to retrieve a Transition ID from a Jira project and provide it to GitLab. If you have the necessary Jira permissions you can attempt to retrieve the Transition ID immediately. Otherwise, you need to open a request ticket and provide the name of the Jira project that you want to enable this feature for. If you already requested and received a Transition ID in your initial request ticket you may skip to step 4.
            Retrieve a Transition ID from a Jira project:
              1. Navigate to your Jira project's workflow page: https://its.cern.ch/jira/plugins/servlet/project-config/project_key/workflows where project_key is the key of your Jira project.
              2. Under the "Action" column press the "Edit" icon.
              3. In the left side of the new screen, switch the view from "Diagram" to "Text" mode. Note the "IDs" inside the parentheses next to their respective workflow transitions under the "Transitions(id)" column.
              4. Once you have acquired a Transition ID, provide it to GitLab in the "Use custom transitions fields" and press `Save changes`.
- **Grant the "Close issues" permissions to the GitLab Service Account**:
    1. Navigate to your Jira project's permissions page: https://its.cern.ch/jira/plugins/servlet/project-config/project_key/permissions where project_key is the key of your Jira project.
    2. Locate the "Close Issues" permission. The right column should list one or more project roles that are granted the "Close Issues" permission for instance "Project Role (Developers)". Most projects will have the "Developers" role here, but some projects might be using another role. Make sure the selected role is also granted the "Browse Projects" permission.
    3. Navigate to your Jira project's roles page: https://its.cern.ch/jira/plugins/servlet/project-config/project_key/roles where project_key is the key of your Jira project.
    4. Edit the "Users" column for the Role identified in step 2 to be allowed to "Close Issues" (in most cases, this will be the Developers role) and add account "GitLab service - gitlab.service@cern.ch (gitlab)".

Note that the issue-closing commit can appear in any GitLab project (including other users' projects).

## How to remove the "Issues" button in GitLab

After JIRA integration is enabled, the "Issues" link on GitLab project pages is still [present by default](https://gitlab.com/gitlab-org/gitlab-ce/issues/33097) and keeps pointing to the GitLab issue tracker. In order to make sure the project users use only Jira and not the GitLab issue tracker, it may be desirable to [disable the GitLab issue tracker for the project](https://gitlab.com/gitlab-org/gitlab-ce/issues/36694#note_38061862): set "Issues" to "Disabled" under "Sharing and permissions" in the project settings.

## How to force commit messages in GitLab to contain a reference to a Jira issue

It is possible to require that every commit pushed to a GitLab project must include a reference to a Jira issue, rejecting any commit not fulfilling that condition.

To enable it, navigate to the "Setting" menu from your GitLab project's main page, open the "Push Rules" settings page and specify in "Commit message" the following regular expression: `[[:upper:]]+\-\d+`. Be mindful that this will not validate that the Jira issue actually exists, but only verify that an issue is mentioned.

## I'm not using the CERN Central Jira instance (its.cern.ch) but a private dedicated instance. How to benefit from these features?

GitLab projects are connected by default to the central Jira instance (its.cern.ch) but it is possible to connect GitLab to a different instance of Jira.

For each GitLab project you wish to connect to a different instance of Jira:

1. From the project's main page (https://gitlab.cern.ch/namespace/project_name), navigate to "Settings" -> "Integrations" -> "JIRA".
2. Replace the URL as appropriate to point to your private instance of Jira. Usually only the server name has to be changed: for instance, replace the URL ("https://its.cern.ch/jira/") with <https://alice.its.cern.ch/jira/>
3. In order to have back-links in Jira when a commit message mentions an issue, and the ability to close Jira issues with the commit message (check paragraphs above), the user will have to create a personal access token with the appropriate access permissions in the private Jira instance, those privileges are specified in the paragraphs above about `Enabling links from a Jira issue` and `Enabling closing a Jira issue`.
