---
title: How to hide projects from the GitLab dashboard?
---

Due to e-group membership, GitLab users may find themselves member of many GitLab projects, resulting in a cluttered Dashboard showing activity of a large number of projects.

In order to be more selective about what projects are shown on the GitLab dashboard, use project "starring": on each project's main page, a star is visible next to the project name. Click the star to add the project to your "starred project list".

On the GitLab dashboard, you can now use the ["Starred Projects" tab](https://gitlab.cern.ch/dashboard/projects/starred) to see the activity from your starred projects only. If you would like to make the "Starred Projects" your default dashboard, follow these steps:

- From the [GitLab dashboard](https://gitlab.cern.ch/), navigate to ["Profile settings"](https://gitlab.cern.ch/profile) then ["Preferences"](https://gitlab.cern.ch/profile/preferences).
- As default dashboard, choose "Starred projects" and Save.
