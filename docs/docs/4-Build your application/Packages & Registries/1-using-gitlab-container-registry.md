---
title: Using GitLab container and package registry
---

## Enable the GitLab container registry

The [GitLab container registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/) is enabled by default for all new GitLab projects created after [September 2016](https://cern.service-now.com/service-portal?id=outage&n=OTG0032967) and available in `gitlab-registry.cern.ch/namespace/project_name:tag`.

For older projects the feature can be enabled manually in the project settings as follows:

1. In your project, go to **Settings > General**.
2. Expand the **Visibility, project features, permissions** section and enable the **Container registry** feature.
3. Click **Save changes**.

Once done the **Packages & Registries** > **Container Registry** entry will appear on the sidebar.

## How to use the GitLab container registry

The GitLab container registry allows users to associate with each GitLab project, Docker images **using the same name as the project** and any number of tags. For instance project `https://gitlab.cern.ch/my_namespace/my_project` will be associated with Docker images `gitlab-registry.cern.ch/my_namespace/my_project[:some_tag]`.

The associated GitLab project with the same name as the image **must exist before** a Docker image can be pushed to the GitLab registry - if a project with the same name does not exist beforehand, pushing the image will result in a permission denied error. In other words, **a Docker image becomes an element of the associated GitLab project** (with same name). The GitLab project does not need to contain any source code: it may contain only Docker images. Permissions to access the Docker image are derived from the permissions on the GitLab project.

The registry entries for a given project can be browsed by clicking in the "Packages & registries" button, available in the top of the project's dashboard. The different tags of the Docker image as well as the commands to login, build and pull or push and the possibility to delete any image are provided.

The GitLab registry is reachable for pull/push operations using server name **gitlab-registry.cern.ch**. Projects and their images **share a common namespace**, so if you have a GitLab project whose URL is `https://gitlab.cern.ch/my_namespace/my_project`, its Docker image will be available in `gitlab-registry.cern.ch/my_namespace/my_project`.

Further information can be found under <https://gitlab.cern.ch/help/user/packages/container_registry/index.md>

### Access a private Gitlab Container Registry

In case the project's visibility is set to private then the Container Registry is private too. If you want to access the Container registry of another repository, for example `repo-A` uses images from `repo-B` to run CI/CD jobs, then you should apply the following settings.

1. Go to repo `repo-B` > **Settings** > **CI/CD**
2. Click expand on **Token Access** and then at **Add Project**
3. Type the full project path including the namespace `test/repo-A` and then **Add Project**
4. Make sure the toggle switch button is enabled.

![Job Token Permission](/img/assets/settings/job_token_access.png)

This is an allow-list that each repo has to give access to other projects to reach them via a [job token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html).
Now `repo-A` pipelines are able to use images from `repo-B` when a user with the required permissions triggers a job.

## Enable the GitLab Package registry

New projects get this feature enabled automatically. You can disable it in case you are not interested, [disable package registry documentation](https://docs.gitlab.com/ee/user/packages/package_registry/index.html#disable-the-package-registry).

For projects created before 15th March 2021, the feature needs to be enabled before being presented in the interface. To do that please follow,

1. In your project, go to **Settings > General**.
2. Expand the **Visibility, project features, permissions** section and enable the **Packages** feature (under **Repository** header).
3. Click **Save changes**.

Once done the **Packages & Registries** > **Package Registry** entry will appear on the sidebar.

## How to use the GitLab package registry

Please have a look to the upstream GitLab documentation at [Package registry documentation](https://docs.gitlab.com/ee/user/packages/package_registry/index.html).

It's worth also having a look to the documentation provided by GitLab under <https://gitlab.cern.ch/help/user/packages/package_registry/index.md>

### Authentication and authorization

Authentication and authorization to push and pull Docker images from the GitLab registry are handled by GitLab: by default, **the same permissions that apply to the project, will apply to its Docker image**. The registry related permissions given to each GitLab project role are covered in the [GitLab permissions matrix](https://gitlab.cern.ch/help/user/permissions.md). For private projects, this means credentials need to be provided in order to pull the Docker image on machines.

Credentials to pull Docker images are typically entered with `docker login gitlab-registry.cern.ch`. However, **Docker saves INSECURELY your CERN credentials to a text file** (see details in the [Docker issue tracker](https://github.com/docker/docker/issues/10318)). For this reason and in order to protect you CERN password, we highly recommend that you do not use your CERN account/password with `docker login`, but instead [generate a new authentication token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with at least `read_registry` scope for each machine where you run docker login. There are multiple types of tokens:

- [Personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) can give read and write access to all docker images your gitlab account has access to, and are typically used on personal computers.
- [Project Access Tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) are similar to passwords, except you can limit access to resources, select a limited role, and provide an expiry date.
- [Group Access Tokens](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html), similar to personal/project access tokens, but associated to a group rather than a project or a user.
- [Job Token](https://gitlab.cern.ch/help/ci/jobs/ci_job_token.md). Allows access to packages in the project running the job for the users running the pipeline. Access to other external projects can be configured.
- [Deploy tokens](https://docs.gitlab.com/ee/user/project/deploy_tokens/#deploy-tokens). They are typically used to deploy an application to a server environment (such as a VM or a container orchestrator like Kubernetes or Openshift).

To protect your credentials, please make sure to use `docker logout` or remove the credentials text file when access is not needed anymore.

A Docker image will be available anonymously (public image) when the associated GitLab project is Public. **Be mindful that such public Docker images (images stored in public GitLab projects) are available anonymously to Internet users** (see [KB0003122](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003122)).

## Building Docker images

GitLab CI can be used to build Docker images and upload them to the GitLab registry or any Docker registry providing a valid Dockerfile in a GitLab project. This mechanism will allow the user to re-build a Docker image and upload it to the Docker registry just by pushing to its Dockerfile hosted in a GitLab project.

Users can run their Docker-build jobs in our default shared GitLab-runners using a Kaniko Docker image. Details on the process and a simple working example of building a Docker image with GitLab CI can be found in the [build_docker_image example project](https://gitlab.cern.ch/gitlabci-examples/build_docker_image/). Also a more complex example where different versions of the same image are generated depending on the branch the user pushes, is available [here](https://gitlab.cern.ch/gitlabci-examples/build_docker_image/blob/build_with_tag_branch/.gitlab-ci.yml).

There is also more examples, such as the one contributed and maintained by our colleagues of the [Linux Service at CERN](https://linux.web.cern.ch/), also using Kaniko and covering ARM architectures. See <https://gitlab.cern.ch/ci-tools/docker-builder> for further details. Please note this approach is not supported by the GitLab Team.

**We strongly recommend running [container scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) for known vulnerabilities on your Docker builds**. Check out recommendation given by our Security Team under [Secure your application](../../5-Secure%20your%20application/index.md), as well as the [official documentation provided by GitLab](https://gitlab.cern.ch/help/user/application_security/index.md). Legacy example can be found under <https://gitlab.cern.ch/gitlabci-examples/container_scanning>.

## Service Level Agreement

- The GitLab integrated container registry is part of GitLab itself so it is supported under the same SLA.
- GitLab registry's data is kept in the S3 service with daily backup to an independent storage.
- The GitLab registry, as it is the case for GitLab, is available outside the CERN network and visible to the Internet. Docker images of GitLab public projects will be publicly available to any anonymous Internet user. More information can be found in [KB0003122](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003122).

## References

- GitLab registry official help pages available [here](https://gitlab.cern.ch/help/user/packages/index.md).
- IT-ASDF presentation (15th September 2016) available [here](https://indico.cern.ch/event/569804/contributions/2304421/attachments/1337634/2012863/ASDF15Sept2016.pdf)
