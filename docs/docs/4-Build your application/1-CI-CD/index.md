---
title: CI/CD
---

## Overview

GitLab features an integrated lightweight [Continuous Integration (CI)](https://en.wikipedia.org/wiki/Continuous_integration) engine. It is recommended for most projects that want to enable Continuous Integration and do not have a complicated CI workflow. If gitlab-ci is not sufficient, the more complex but more featureful [Jenkins CI](https://jenkins.docs.cern.ch/) can be considered.

GitLab CI allows to run a number of tasks (such as build, unit tests, possibly deploy tasks) automatically whenever new code is pushed to your GitLab project. The status of the tasks (e.g. whether build was successful or not) is then shown next to the commit. Merge Requests are also decorated with CI results, helping in deciding whether a Merge Request can be merged or not.

Details about GitLab CI can be found in the CI section of the [GitLab help](https://gitlab.cern.ch/help). This article only highlights the most important points and what is specific to the CERN setup.

## How to enable GitLab CI

To enable GitLab CI, simply add a file named `.gitlab-ci.yml` at the root of your project. See the [configuration reference for this file](https://gitlab.cern.ch/help/ci/yaml/index.md).

New links will then show up in your project's main page ("Builds") and in the project's settings ("Runners", "Variables", "Triggers" etc.) to further configure CI. In case they do not show up, visit the project "Settings" and check that the "Builds" feature is enabled (on the main page of the project settings).

The project must then be associated with "runners", i.e. worker nodes that will perform the actual tasks: build code, run unit tests etc. Some basic ["shared runners"](./1-Runners/index.md) are provided, available for all projects to run CI tasks. Note that it is also possible to use "specific runners": machines dedicated to your projects that you need to provide and configure yourself.

To enable the default "shared runners", visit your project's "Runner settings" (under "CI/CD" Settings), and click the button "Enable shared runners for this project" if there is one (if shared runners are enabled already, as should be the case by default, there will instead be a button "Disable shared runners for this project"). Shared runners execute tasks within Kubernetes containers.

## Docker images when using Shared Runners

Shared runners execute tasks within [Kubernetes containers](https://kubernetes.io/docs/concepts/containers/). The [Docker image](https://gitlab.cern.ch/help/ci/docker/using_docker_images.md#what-is-an-image) used to run a task is controlled by the image setting in the `.gitlab-ci.yml` file. 

By default, jobs will use an lighweight AlmaLinux9 image, provided by our colleagues from the [Linux Team](https://gitlab.cern.ch/linuxsupport), with very basic development tools. The shared runners can however be expected to receive some improvements based on user feedback and progress of official Docker images for CERN operating systems.

In addition, images from the [Docker Hub](https://hub.docker.com/) can also be used.  

If your job relies on images that reside in [Docker Hub](https://hub.docker.com/), consider using the [DockerHub cache](https://clouddocs.web.cern.ch/containers/registry/quickstart.html#dockerhub-cache) provided by the CERN Cloud team in order to circumvent the [DockerHub download rate limit](https://docs.docker.com/docker-hub/download-rate-limit/).

## Examples

Some examples about using GitLab-CI at CERN (e.g. using CVMFS, Kerberos or manipulating files on AFS or EOS) can be found in <https://gitlab.cern.ch/gitlabci-examples>.

GitLab also provides a [detailed tutorial](https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/) and [a number of generic templates](https://gitlab.com/gitlab-org/gitlab-ci-yml) for various frameworks and programming languages.

## Using GitLab CI

### Using custom Docker images

Public Docker images available in the Docker Hub may require many [`before_script`](https://docs.gitlab.com/ee/ci/yaml/#before_script) actions to install all the dependencies required for running a particular job (typically `yum -y install dep1 dep2 ...`). Re-doing all these actions with each job execution may make jobs too slow. Using a local installation of Docker, it is possible [to create your own Docker images](https://docs.docker.com/engine/articles/dockerfile_best-practices/) prepared with all the dependencies and configuration required for your jobs, thus speeding up job execution. [An example is provided](https://gitlab.cern.ch/gitlabci-examples/custom_ci_worker) showing how to extend the default CI worker image with extra libraries (see also section "Building Docker Images").

Once available in `gitlab-registry.cern.ch`, a custom image can be referenced using name `gitlab-registry.cern.ch/namespace_name/project_name[:tag]` in a job's image setting. GitLab CI can use private images stored in the GitLab registry.

Currently the maximum size a custom image can have is `10GB` when uncompressed. Be aware that any job using an image larger that this may fail on the shared runners.

### Building Docker images

It is possible to automatically build a docker image by pushing its Dockerfile to a GitLab repository and upload the image generated to either the GitLab container registry or to [Harbor](https://kubernetes.docs.cern.ch/docs/registry/quickstart/). Check our [GitLab CI examples](https://gitlab.cern.ch/gitlabci-examples) to get inspired.

For further information about building Docker images, refer to our [Packages and Registries - Building Docker images](../Packages%20&%20Registries/1-using-gitlab-container-registry.md#building-docker-images) documentation.

### Using secrets in CI jobs (passwords, keytabs, X509 certificates...)

It may be necessary for tests and deployment tasks to have secrets such as username/password, a X509 certificate for Grid service access, a Kerberos keytab etc. **These must NOT be included in the project's source code** or the `.gitlab-ci.yml` file. Instead, they can be provided as [Secure Mask Variable](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) or as a [Secure Files](https://docs.gitlab.com/ee/ci/secure_files/index.html).

:::tip

To make these variables more secure, consider using [external secrets](https://docs.gitlab.com/ee/ci/secrets/index.html) and [file type variables](https://docs.gitlab.com/ee/ci/variables/#use-file-type-cicd-variables) to prevent commands such as `env/printenv` from printing secret variables.

:::

Secure Variables are managed from the CI/CD variables page in a project's settings. These variables are then available as environment variables in the job. If necessary, write them into a file using for instance:

```yaml
before_script:
  - echo "$⁠{SECRET_VARIABLE_NAME}" > my.file
```

Binary files may be base64-encoded before being saved as a Secure Variable and written with:

```yaml
script:
- echo "$⁠{SECRET_VARIABLE_NAME}" | base64 -d > my.file
```

### Review Builds and their status

All builds for your project can be reviewed in the Builds section of your project (from the left navigation bar on the project's main page). Builds can be cancelled from there as well.

Build status will also be shown next to commits and Merge Requests. For Merge Requests issued from a fork, CI must be enabled in the fork for CI status to be available.

## Shared Runners

For the list of Shared Runners available at CERN, please refer to [Runners](./1-Runners/index.md).

## Custom (Private) Runners

If the basic "shared runners" provided are not sufficient, you may consider deploying your own GitLab CI runners. The GitLab help has [documentation about setting up a GitLab CI "specific runner"](https://gitlab.cern.ch/help/ci/runners/index.md) dedicated to your project. The "gitlab-ci coordinator URL" for CERN's instance of GitLab is `https://gitlab.cern.ch`.

:::caution

Please note that Git Service does not provide any kind of support for Custom (private) runners. Users experiencing issues may have to contact the owners and/or responsibles of the custom runner.

:::

### Custom Runners on Linux Machines

A CERN mirror of the repository for installing gitlab-ci on CC7 can be found at <https://linuxsoft.cern.ch/mirror/packages.gitlab.com/runner/gitlab-ci-multi-runner/>

:::caution

Please note that end-of-life for CC7 OS is next Summer 2024 as per [OTG0145248](https://cern.service-now.com/service-portal?id=outage&n=OTG0145248)

:::

### Custom Runners on Windows Machines

Check the following official documentation to [install GitLab Runners on Windows](https://docs.gitlab.com/runner/install/windows.html).

### Custom Runners via Puppet-managed Linux machines

Puppet machines may include `cern_gitlab::gitlab_ci_runner` to install a GitLab CI runner. This will only install the runner service; it still has to be [registered with your GitLab project (via a token)](https://docs.gitlab.com/runner/register/) and [configured](https://docs.gitlab.com/runner/configuration/advanced-configuration.html) as appropriate. If using Docker together with Puppet, make sure to apply the necessary [prerequisites for Docker in your Puppet manifests](https://configdocs.web.cern.ch/configdocs/misc/docker.html#prerequisties). 

### Using the same runner in multiple projects

Note that after adding a specific runner to one of your projects, it is possible to re-use it in another project: the "Runners settings" page in the GitLab project's settings (under "CI/CD" settings) will show existing runners enabled in any other project that you have `Owner` access to, and provide the option to enable them for this project as well.
