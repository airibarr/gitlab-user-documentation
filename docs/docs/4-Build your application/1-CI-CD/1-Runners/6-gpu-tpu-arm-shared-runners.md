---
title: Shared Runners for GPU, TPU and ARM
---

GitLab CI jobs may have access to GPUs, TPUs and ARM architecture using public cloud resources. These experimental runners are not supported via the official GitLab support channel - please see [gitlabci-examples / GPU TPU and ARM on Public Clouds](https://gitlab.cern.ch/gitlabci-examples/gpu-tpu-arm-public-clouds) for help and contact information.
