---
title: Kubernetes+executor Shared Runners
---

A new fleet of runners have been deployed at CERN recently, centrally managed and using what is called the [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html).

K8s Default Runners are meant to be used for common workflows such as build/push images using Kaniko, execute simple and/or advanced workflows to generate an output, etc. However, these runners **cannot be used together with Docker or any similar alternative**, since these runners run in a non-privilege mode, and therefore there is no access to the host. For such, check the [Docker Privileged Runners](./4-docker-privileged-runners.md) section.

The goal of these runners was to replace the Docker+executor Shared runners recently running and decommissioned as of October the 2nd. During the transition from the Docker+executor Shared Runners to Kubernetes+executor Shared Runners, users needed to play with [tags](https://docs.gitlab.com/ee/ci/yaml/index.html#tags) in order to validate their jobs when landing in this new fleet of runners.

## Usage

For the case of K8s Shared Runners, use one of the following tags:

- `no tag`: as of 24th August 2023, all jobs with no tag specified will land in the new Runners infrastructure based on Kubernetes+executor.
- `k8s-default`
- `k8s-default-runners`

As an example, you will use:

```yaml
job:
  script:
  - echo "do something..."
```

Or:

```yaml
job:
  tags:
  - k8s-default
  script:
  - echo "do something..."
```

For examples of CI job definitions using the various runners, see <https://gitlab.cern.ch/gitlabci-examples> (please sign in with a CERN account to see all of them).

## General considerations for Kubernetes+executor Shared Runners

| Specs                 | Value                     |
| --------------------- | ------------------------- |
| Worker specs (21x)    | 16 vCPU - 28.7 GiB RAM    |
| Concurrent jobs (max) | 120                       |
| Default image         | AlmaLinux9                |
| CVMFS available       | ❌                        |
| Privileged            | ❌                        |
| Runner configuration  | [K8s Default Runners](https://gitlab.cern.ch/vcs/gitlab-runners/-/blob/master/chart/environments/k8s-prod-v25.yaml) |

These K8s Shared Runners use the [`Kubernetes-executor`](https://docs.gitlab.com/runner/executors/kubernetes.html). This means that jobs executed by these runners will run on a dedicated Kubernetes cluster, aiming at improving scalability, resource optimization, isolation and portability.

These new type of runners use a special feature gate in the Kubernetes cluster called `UserNamespacesStatelessPodsSupport`, that can be used as of GitLab `15.10`. UserNamespacesStatelessPodsSupport (also known as User Namespace Remapping) is a feature in Kubernetes that allows users to run pods and containers with different user and group IDs than the host system, by mapping them to different user namespaces. This is important because running containers as root on the host system can be a security risk, as any vulnerabilities in the container or application could potentially lead to the entire system being compromised. By using `UserNamespacesStatelessPodsSupport`, users can isolate the containers and their workloads from the host system, improving security.

However, there is a drawback of using this feature gate. Those users aiming at mounting/using a volume such as `cvmfs`, won't be able to run jobs in these specific runners. GitLab Service is assessing other alternatives for this specific use case, and further information will be provided.

Ragarding the image, these runners use the **AlmaLinux9** image by default. Users are allowed to replace the image at their best convenience, as it was the case nowadays.

An example of using an image other than the default one provided is:

```yaml
job:
  image: gitlab-registry.cern.ch/my-image
  script:
  # rest of things
```

Support for accessing `cvmfs` is not provided. For this case, refer to [K8s CVMFS Runners](./3-cvmfs-runners.md).

These runners have the [Interactive Web Terminals](https://docs.gitlab.com/ee/ci/interactive_web_terminal/) enabled, allowing users to debug during the CI job runtime. As of April 2020, the web terminal is only available for the duration of the job: to debug why a specific command fails in the CI job script, it may be necessary to replace the failing command to troubleshoot with a `sleep` command and run the failing command interactively in the web terminal while the main job sleeps.

## Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

If you need to run these pipelines, please deploy your own private runners to avoid affecting the rest of the users.

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
