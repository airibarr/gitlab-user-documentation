---
title: Kubernetes+executor ARM Runners
---

A new fleet of runners have been deployed at CERN recently, centrally managed and using what is called the [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html), targeting ARM-based workloads.

GitLab ARM runners are specialized runners that can execute CI/CD jobs on ARM-based devices. ARM-based devices are becoming increasingly popular for a variety of workloads, including servers, cloud computing, and edge computing. GitLab ARM runners allow developers to build, test, and deploy software for ARM-based devices quickly and efficiently.

These runners are meant to be used for common workflows such as building and testing ARM-based binaries, deploying ARM-based applications to ARM-based devices, etc.

## Usage

In order to start using the K8s ARM Runners, a user needs to add one the tags mentioned below to its `.gitlab-ci.yml` configuration file.

- `k8s-arm`
- `docker-aarch64`

As an example, you will use:

```yaml
job:
  tags:
  - k8s-arm
  script:
  - echo "do something..."
```

Or:

```yaml
job:
  tags:
  - docker-aarch64
  script:
  - echo "do something..."
```

For examples of CI job definitions using the various runners, see <https://gitlab.cern.ch/gitlabci-examples> (please sign in with a CERN account to see all of them).

## General considerations for Kubernetes+executor ARM Runners

| Specs                 | Value                     |
| --------------------- | ------------------------- |
| Worker specs (1x)     | 20 vCPU - 57.3 GiB RAM    |
| Concurrent jobs (max) | 6                         |
| Default image         | AlmaLinux9                |
| CVMFS available       | ✅                        |
| Privileged            | ❌                        |
| Runner configuration  | [K8s ARM Runners](https://gitlab.cern.ch/vcs/gitlab-runners/-/blob/master/chart/environments/k8s-mono-arm-prod-v26.yaml) |

These K8s ARM Runners use the [`Kubernetes-executor`](https://docs.gitlab.com/runner/executors/kubernetes.html). This means that jobs executed by these runners will run on a dedicated Kubernetes cluster, aiming at improving scalability, resource optimization, isolation and portability.

In October 2023, K8s ARM Runners cluster is limited on capacity, having only 1 ARM worker node, able to handle `6` concurrent jobs at a time. Plans for scaling up the cluster is underway.

Regarding the image, these runners use the **AlmaLinux9** image by default (concretely `gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9:latest`). Users are allowed to replace the image at their best convenience, as it was the case nowadays by setting the [`image`](https://docs.gitlab.com/ee/ci/yaml/#image) keyword appropriately.

An example of using an image other than the default one provided is:

```yaml
job:
  image: gitlab-registry.cern.ch/my-image
```

Support for accessing `cvmfs` is provided, since every job will have mounted a volume under the `/cvmfs` path.

These set of K8s ARM Runners will run as **`non-privileged`**, meaning that `docker` commands are unlikely to work. Plans for K8s ARM Privileged Runners are being analyzed.

These runners have the [Interactive Web Terminals](https://docs.gitlab.com/ee/ci/interactive_web_terminal/) enabled, allowing users to debug during the CI job runtime. As of April 2020, the web terminal is only available for the duration of the job: to debug why a specific command fails in the CI job script, it may be necessary to replace the failing command to troubleshoot with a `sleep` command and run the failing command interactively in the web terminal while the main job sleeps.

## Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

If you need to run these pipelines, please deploy your own private runners to avoid affecting the rest of the users.

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
