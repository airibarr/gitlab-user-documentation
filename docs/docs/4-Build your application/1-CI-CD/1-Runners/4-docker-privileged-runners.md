---
title: Docker+machine Privileged runners
---

These shared runners can provide full access to a Docker daemon via [Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor). This allows to use shared runners for CI tasks that could not be accomplished with the other shared runner types and previously required GitLab users to set up dedicated runners, at the cost of somewhat slower jobs because a virtual machine is provisioned for each CI job.

In particular, these runners enable running Docker-in-Docker (for any version of Docker) or scenarios like [GitLab Auto-DevOps](https://docs.gitlab.com/ee/topics/autodevops/). For examples, see <https://gitlab.cern.ch/gitlabci-examples/demo-privileged-runners>.

These runners take some time to provision a new virtual machine where your job runs: in order to preserve isolation between projects, these runners must start a new virtual machine for each CI job. Each job must wait for the single-use VM to be started and Docker images cannot be cached to improve job start time. We measure CI jobs take about 1min30s - 3min for provisioning before running your scripts, and thus these runners should only be used for jobs that cannot be executed on the other runner types.

## Usage

In order to run your job on these runners, use tag `docker-privileged-xl`. The job will run on a virtual machine with with 8 virtual CPU, 16GB of memory and 80 GB of disk space.

As an example, you will use:

```yaml
job:
  tags:
  - docker-privileged-xl
  script:
  - echo "do something..."
```

For examples of CI job definitions using the various runners, see <https://gitlab.cern.ch/gitlabci-examples> (please sign in with a CERN account to see all of them).

## General considerations for Docker+machine Privileged Runners

| Specs                 | Value                     |
| --------------------- | ------------------------- |
| Worker specs (10x)    | 8 vCPU - 16 GiB RAM       |
| Concurrent jobs (max) | 1 x VM                    |
| Default image         | CC7                       |
| CVMFS available       | ✅                        |
| Privileged            | ✅                        |

These runners allow to mount CVMFS as per [our CI examples](https://gitlab.cern.ch/gitlabci-examples/demo-privileged-runners/blob/527ab857/.gitlab-ci.yml#L65-93), however it is **strongly recommended** for those using `cvmfs` to make use of the [CVMFS Runners](./3-cvmfs-runners.md) for such purpose.

Please note that due to the per-job VM provisioning requirement, these runners will not be available whenever Openstack Nova API is unavailable.

## Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

If you need to run these pipelines, please deploy your own private runners to avoid affecting the rest of the users.

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
