---
title: GitLab Runners at CERN
---

GitLab Runners are agents that run GitLab CI/CD jobs. They can be installed on any machine, including your local machine, a virtual machine, a server, or in a cloud environment.

GitLab Runners are responsible for executing the tasks defined in GitLab CI/CD pipelines. These tasks can include building and testing code, deploying applications, and running other automated tasks.

GitLab Runners are registered with a GitLab instance, and they communicate with the GitLab instance to request and receive jobs. When a GitLab CI/CD pipeline is triggered, GitLab Runner will be assigned a job, and it will execute the job according to the instructions in the pipeline.

GitLab Runners can be shared by multiple projects (`instance` runners or `group` runners), or they can be dedicated to a single project (`project` runners). Among them, some of them can be publicly available, or privately restricted.

In this documentation, we will explain and focus on how the `instance` runners, publicly available, are made of and how you can configure them to use.

## Instance Runners

Instance runners are a type of GitLab runner that is registered with a GitLab instance. They are different from other types of runners, such as `project` runners and `group` runners, in that they are not associated with any specific project or group.

Instance runners are typically used to run jobs that are shared by multiple projects or groups, or to run jobs that require access to resources that are not available to other types of runners, such as specialized hardware or software.

They are what we call the `Shared Runners`.

### Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

Should to run these pipelines is needed, it must be considered to deploy your own private runners to avoid affecting the rest of the users.

### Capabilites

At CERN there are currently 4 types of shared runners centrally provided by the Git Service, and available for all GitLab projects using GitLab CI. A runner is selected by specifying one or more tags in CI job definitions.

These type of runners are:

- [**K8s Shared Runners**](./2-k8s-shared-runners.md).
- [**K8s CVMFS Runners**](./3-cvmfs-runners.md).
- [**K8s ARM Runners**](./7-k8s-arm-runners.md).
- **K8s Privileged Runners**: coming soon.
- [**Docker Privileged Runners**](./4-docker-privileged-runners.md)

For examples of CI job definitions using the above runners, see <https://gitlab.cern.ch/gitlabci-examples> (please sign in with a CERN account to see all of them).

There are other types of runners too, not centrally provided by the Git Service, but widely used for the community at CERN, such as:

- [**Docker MultiArch Runners**](./5-docker-multiarch-runners.md)
- [**GPU Runners**](./6-gpu-tpu-arm-shared-runners.md)
  
When shared runners are not appropriate for specific CI tasks, GitLab users may consider deploying and maintaining their own (private) dedicated runners. See the "dedicated runners" section in [KB0003690](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003690).

### General considerations

Shared Runners use either `Kubernetes` or `Docker`, IPv4-only container network. Shared runners are provisioned dynamically from the CERN Cloud.

Git Service is doing an effort into decommissioning the `Docker+machine` executor runners, in favor of the `Kubernetes` ones, as explained in the [GitLab Runners cleaning up campaign - OTG0078219](https://cern.service-now.com/service-portal?id=outage&n=OTG0078219), taking also the opportunity for users to modenise their workflows.

Below you will find general considerations for each.

#### Kubernetes Runners

Kubernetes executor runners are GitLab runners that use the Kubernetes executor to execute jobs. The Kubernetes executor is a special type of executor that provisions a Kubernetes pod to run each job.

This allows CERN users to run jobs on a Kubernetes cluster, which can provide several benefits, including scalability, isolation and reproducibility.

These runners use **AlmaLinux 9** images as the default base image (concretely the `gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9:latest`) provided and maintained by our colleagues from the [Linux Service](https://linux.web.cern.ch/). The approach in these runners is slightly different than the one offered in the Docker runners. Linux Service provides a lightweight image, allowing users to add tools or packages they need on their own. This image is also re-built regularly (at least every 1st day of the month) to stay up-to-date. This approach reduces load on the Gitlab Service, potential user errors and simplifies migrations from one OS to another.

#### Docker Runners

`Docker+machine` executor runners (commonly called `Docker runners`), have been used for long. They serve for the same purpose as the Kubernetes runners, but the underlying technology is different. This is a project forked by GitLab from Docker, that they keep maintaining nowadays, although there are plans to decommission them in the main instance GitLab.com.

Docker runners use CC7 images as the base image (concretely the `gitlab-registry.cern.ch/ci-tools/ci-worker:cc7` image), provided and maintained by the Git Service. This image is based on a CC7 base image with some additional tools and packages installed over the time. They are re-built and re-generated every month in order to stay up-to-date in terms of new versions of packages and security vulnerabilities.

The are maintained in order to keep the [Docker Privileged Runners](../1-Runners/4-docker-privileged-runners.md) working, although plans about its replacement are on the table and being evaluated.

In both cases, the maximum size of a job's log output is **4MB**. Jobs producing large output may consider redirecting output to files and save them as [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#job-artifacts) (NB: artifacts expire after 30 days by default). As of GitLab 12.5, these job artifacts can be conveniently [exposed directly on the Merge Request page](https://docs.gitlab.com/ee/ci/yaml/index.html#artifactsexpose_as).
