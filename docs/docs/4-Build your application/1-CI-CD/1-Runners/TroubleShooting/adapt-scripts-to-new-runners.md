# Adapt scripts to run in new Kubernetes runners

Several users are experiencing a confusing behaviour when running their jobs either in the old runners or in the new ones. Their jobs are/were working properly in the old runners based on Docker, however at the time of moving to the new runners based on Kubernetes, errors arise.

This can be related to multiple things, and it is therefore that we recommend users to submit a ticket with the Git Service in [here](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&se=git-service).

Below we will explain some possible causes and solutions that may help you to identify your issues and to solve them, eventually moving to the new runners.

## Using python scripts

Runners based on Docker run with a CC7 image by default. CC7 images come with `python` (version `2.X`) installed. On the contrary, new runners based on Kubernetes run with a Alma9 image, having `python3` (version `3.X`) installed by default.

Therefore, whether you have your jobs similar as follows:

```yaml
job:
  script:
  - python <some arguments>
```

This snippet will work for old runners based on CC7, but not for new runners based on Alma9.

There are 2 possibilities here to fix it:

- Adapt your scripts to be python3-compliant. **RECOMMENDED**
- Use a CC7 image in the new runners.

### Adapt your scripts to be python3-compliant

In order to adapt your scripts to be python3-compliant, you just need to change your scripts as follows in your `.gitlab-ci.yml` file of your repository.

Following the example above:

```yaml
job:
  script:
  - python3 <some arguments>
```

This will allow you to start running your scripts in the new runners based on Kubernetes based on Alma9.

### Use a CC7 image in the new runners

While it is possible for you to just keep using the new runners with CC7 images, the Git Service team does not recommend it. CC7 images are meant to be phased out as of summer 2024, therefore we understand it is a good occassion to clean up and polish your `.gitlab-ci.yml` files in order to modernize them all.

In this case, and **whether it is strictly necessary to keep using CC7 images**, refer to the example below:

```yaml
job:
  # Set the image name of your job to the CC7 image
  image:
    name: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
  - python <some arguments>
```

This will allow your jobs to run in runners based on Kubernetes without too much of a trouble.

## FAQ

**Q: Even following the above my jobs are still stuck/failing. What should I do?**

First thing we use to recommend users is to ensure their jobs are landing in the old runners infrastructure. This can be achieved with the tag `docker` as follows:

```yaml
job:
  tags:
  - docker
  script:
  - python <some arguments>
```

Then, we strongly recommend you to open a ticket with us for validation and to try to find the root cause of your fails to move your jobs eventually to the new runners.

**Q: In old runners, I've been using tools such as `mailx`, that came by default with the CC7 images. In new runners, using Alma9, I cannot find such tool. What should I do?**

It has been noticed that some of the tools provided in CC7 are not present in Alma9, such as `mailx`. In this case, and as per <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/considerations_in_adopting_rhel_9/assembly_changes-to-packages_considerations-in-adopting-rhel-9>, a series of changes have been introduced and several tools/packages need to be adapted.
For the case of `mailx`, the tool `s-nail` is the appropriate one to be used in Alma9 images.

**Q: Great, but some time ago, these tools came installed by default in CC7 images, and now I cannot see them installed in Alma9 images**.

This is expected. From now on, our [Linux Team](https://linux.web.cern.ch/) is the one providing the Alma9 image, and this image is built very basic on purpose to avoid installing packages here and there. Different OS versions may have different default packages and provide different tools, hence **you should make sure you explicitly install/declare your dependencies to avoid surprises when migrating**.

Following the above example of `mailx`.

In CC7, an example of the use of this tool can be:

```yaml
job:
  image:
    name: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
  - mailx ...
```

In Alma9, the same should be:

```yaml
job:
  # no image specified, therefore defaulting to gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9:latest in new Kubernetes runners
  # before_script can be used in this case to pre-install tools/packages needed.
  before_script:
  - yum install -y s-nail
  script:
  # - yum install -y s-nail, whether you prefer using the script keyword instead of the before_script
  - s-nail ...

```

---

Not working? Please submit a ticket with us [through the Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
