---
title: Honor Dockerfile entrypoint in K8s Shared Runners
---

In the new [K8s Shared Runners](../2-k8s-shared-runners.md) there is a new feature that controls whether to the Docker entrypoint of an image can be honored or not when running jobs on Kuberntes. By default, this flag is set to `false` (or `0`), which means that the runner will ignore the entrypoint and instead execute the job's script directly. When `FF_KUBERNETES_HONOR_ENTRYPOINT` is set to true, the runner will first attempt to execute the image's entrypoint. If the image does not have an entrypoint defined, or if the entrypoint fails to execute, the runner will then execute the job's script.

Enabling `FF_KUBERNETES_HONOR_ENTRYPOINT` can be useful for jobs that rely on the image's entrypoint to perform some initial setup or configuration. For example, if an image has an entrypoint that starts a web server, enabling this flag will ensure that the web server is started before the job's script is executed.

See further information about this feature flag under [GitLab Runner feature flags](https://docs.gitlab.com/runner/configuration/feature-flags.html).

:::info

This feature flag was not present in the old runners based on Docker+machine executor. The behaviour, by default back then, was to always honoring the Docker entrypoint.

:::

## Example configuration

This can be seen as a new feature, since it provides flexibility, but sometimes can lead into issues that are hard to identify and to solve. Have a look to our examples below.

Let's imagine that you want to honor the Docker entrypoint of a specific job, but not all of them in your `.gitlab-ci.yml` definition. We need to specify the `FF_KUBERNETES_HONOR_ENTRYPOINT` feature flag, either at the level of the [group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#for-a-group), or per job-basis, as the example below. You could also to override it at the project or job level if needed by redefining the variable.

```yaml
# We set the variable globally
variables:
  FF_KUBERNETES_HONOR_ENTRYPOINT: 1

# A random job in which we want to honor the Docker entrypoint of the image.
# This will inherit the value of FF_KUBERNETES_HONOR_ENTRYPOINT (set to 1 (true))
my first job honoring docker entrypoint (will success):
  stages: build
  image:
    name: gitlab-registry.cern.ch/some/random/image/to/honor/entrypoint
    entrypoint: [""]
  script:
    - echo "Do something..."

# A random job in which we want NOT to honor the Docker entrypoint of the image.
my second job NOT honoring docker entrypoint (will success):
  stages: build
  # default image
  script:
    - echo "Do something..."

#
## Special case ##
# Build and image, example extracted from https://gitlab.cern.ch/gitlabci-examples/build_docker_image/-/blob/master/.gitlab-ci.yml
# This will inherit the value of FF_KUBERNETES_HONOR_ENTRYPOINT (set to 1 (true))
build and image (will fail):
  stages: build
  variables:
    IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}:latest
    image: 
      # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
      name: gcr.io/kaniko-project/executor:debug
      entrypoint: [""]
    script:
      # Prepare Kaniko configuration file
      - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      # Build and push the image from the Dockerfile at the root of the project.
      - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $IMAGE_DESTINATION
      # Print the full registry path of the pushed image
      - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
```

As mentioned above, and after setting the `FF_KUBERNETES_HONOR_ENTRYPOINT` feature flag globally, this will affect to all jobs in our configuration, provoking some of them to work, some of them to fail. Error looks similar to:

```bash
...
Waiting for pod gitlab/runner-5gwuywh-project-7515-concurrent-24ncn7 to be running, status is Pending
	ContainersNotReady: "containers with unready status: [build helper]"
	ContainersNotReady: "containers with unready status: [build helper]"
ERROR: Job failed (system failure): prepare environment: setting up trapping scripts on emptyDir: unable to upgrade connection: container not found ("build"). Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information
```

To fix it, user need to set explicitly on a per job basis the value of `FF_KUBERNETES_HONOR_ENTRYPOINT` to false (0), as follows:

```yaml
# We set the variable globally
variables:
  FF_KUBERNETES_HONOR_ENTRYPOINT: 1

# A random job in which we want to honor the Docker entrypoint of the image.
# This will inherit the value of FF_KUBERNETES_HONOR_ENTRYPOINT (set to 1 (true))
my first job honoring docker entrypoint (will keep succeeding):
  stages: build
  ...

# A random job in which we want NOT to honor the Docker entrypoint of the image.
my second job NOT honoring docker entrypoint (will keep succeeding):
  stages: build
  ...

# Build and image, example extracted from https://gitlab.cern.ch/gitlabci-examples/build_docker_image/-/blob/master/.gitlab-ci.yml
# This will inherit the value of FF_KUBERNETES_HONOR_ENTRYPOINT (set to 1 (true))
build and image (will now succeed):
  stages: build
  variables:
    FF_KUBERNETES_HONOR_ENTRYPOINT: 0 # feature flag set to 0, overriding the global value.
    IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}:latest
    image: 
      # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
      name: gcr.io/kaniko-project/executor:debug
      entrypoint: [""]
    script:
      # Prepare Kaniko configuration file
      - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      # Build and push the image from the Dockerfile at the root of the project.
      - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $IMAGE_DESTINATION
      # Print the full registry path of the pushed image
      - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
```

Now, all your jobs will succeed as expected, one of them honoring the Docker entrypoint, others don't. Relevant issue why this is now possible can be found [here](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28914)

:::warning

If after honoring or not the entrypoint of your Dockerfile with the `FF_KUBERNETES_HONOR_ENTRYPOINT` feature flag your jobs keep showing an error like the one above about `ERROR: Job failed (system failure): prepare environment: setting up trapping scripts on emptyDir ...`, it must be considered reviewing your Dockerfile.

Note that in the new Kubernetes+executor runners, there are known limitations (see <https://docs.gitlab.com/runner/executors/kubernetes.html#container-entrypoint-known-issues> for futher information), especially regarding the use of environment variables (defined under the CI/CD variables of the GitLab UI) in the entrypoint of a Dockerfile (approach workable in the old Docker runners). Using the `FF_KUBERNETES_HONOR_ENTRYPOINT` feature flag set to `true` or `false` will fail in any case.

This is a common issue from users coming from the Docker runners, and workarounds proposed go for moving away the excerpt of the code that references the environment variables in the entrypoint, and inject this code into the `.gitlab-ci.yml` file either in the `before_script` or `script` keywords section.

A feature proposal has been opened by the Git Service at CERN with GitLab to see whether they can provide with a solution:

- <https://gitlab.com/gitlab-org/gitlab/-/issues/424246>

:::

## References

Decisions about why this feature flag has been offered and fixes around it can be found under the following upstream issues:

- [Kubernetes executor ignores Docker entrypoint](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125).
- [If FF_KUBERNETES_HONOR_ENTRYPOINT=true, jobs with no entrypoint defined hang during script execution](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28914).

---

Not working? Please submit a ticket with us [through the Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
