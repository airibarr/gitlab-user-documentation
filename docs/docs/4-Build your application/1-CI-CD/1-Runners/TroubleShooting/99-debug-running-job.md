---
title: Debug a running job
---

The debug terminal for running jobs in GitLab is a web terminal that allows you to access a shell in GitLab for running one-off commands for your CI pipeline. It is useful for debugging jobs that are failing or experiencing unexpected behavior.

To access the debug terminal, go to the job page in GitLab and click the Debug button. This will open a new terminal window where you can run commands. The debug terminal has access to the same environment as the GitLab runner, so you can use it to inspect the state of your job and troubleshoot any problems.

Official documentation can be found under [Debugging a running job](https://docs.gitlab.com/ee/ci/interactive_web_terminal/#debugging-a-running-job).

:::warning

There is a known issue in the Debug Terminal, preventing CI/CD variables to show up. Whenever you are using environment variables set as part of the CI/CD variables in the UI, note that those won't be available in the Debug Terminal. They will be only available in the job's execution.

Follow up issue under <https://gitlab.com/gitlab-org/gitlab/-/issues/424246#note_1549393251>

:::
