---
title: Yum install command hangs when using CC7 images in the new runners
---

:::info

As of 05th September 2023, Git Service has decided to inject this limit on all jobs by default in the new runners infrastructure based on Kubernetes. **No actions needed** from the users point of view.

Users will start seeing the following message when running their jobs:

```bash
$ # INFO: Lowering limit of file descriptors for backwards compatibility. ffi https://cern.ch/gitlab-runners-limit-file-descriptors # collapsed multi-line command
```

See further information in [OTG0079605](https://cern.service-now.com/service-portal?id=outage&n=OTG0079605)

:::

This is known issue, and it is related to the high limit on the number of files set by Kubernetes (also in Docker, although not enough noticed). **It is an issue known specifically for CC7 images** in combination with this high number of file descriptors, while it has not been spotted so far for any other image based on CS8, Alma9, or RHEL9. See #Misc section for further information about it.

:::warning

**CC7** End of Life (EOL) is scheduled for next June 2024. Consider updating your image to a most recent OS, such as Alma9, whenever possible.

**CS8** Enf of Life (EOL) is scheduled for September the 30th 2023. Further information under [OTG0079474](https://cern.service-now.com/service-portal?id=outage&n=OTG0079474)

:::

To fix it, we need to relax the limit of file descriptors either in our `.gitlab-ci.yml` file, or in the `Dockerfile`:

To our `.gitlab-ci.yml` file could look like:

```yaml
job:
  # Add the before script section, adapting the number of open files limit with the `ulimit -n` command.
  before_script:
    - ulimit -n 1048576 # or ulimit -n 1024
  script:
    - yum install ...
    # rest of things
```

or

```yaml
job:    
  script:
    # Adapt the number of open files limit with the `ulimit -n` command directly in the script keyword.
    - ulimit -n 1048576 # or ulimit -n 1024 
    - yum install ...
    # rest of things
```

In our `Dockerfile`, it could look like:

```dockerfile
FROM <image>

# or ulimit -n 1024
RUN ulimit -n 1048576 && yum install ...

RUN ...
```

That's all. Your `yum install` command should be able to work as expected.

## Misc

In recent versions of Kubernetes (concretely observed as of `version >= 1.25`), `containerd`, the container runtime used with Kubernetes, has updated their configuration, reflecting an increase of the limit of the file descriptors (a.k.a. `LimitNOFILE`) used, as spotted in:

```
systemctl cat containerd.service

# /etc/systemd/system/containerd.service
...
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNPROC=infinity
LimitCORE=infinity
LimitNOFILE=1048576 # < 1.25
LimitNOFILE=Infinity # >= 1.25
```

This provokes issues as the one stated above, and bugs about it have been reported in the following issues:

- <https://bugzilla.redhat.com/show_bug.cgi?id=1537564>
- <https://stackoverflow.com/questions/74345206/centos-7-docker-yum-installation-gets-stuck>

---

Not working? Please submit a ticket with us [through the Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
