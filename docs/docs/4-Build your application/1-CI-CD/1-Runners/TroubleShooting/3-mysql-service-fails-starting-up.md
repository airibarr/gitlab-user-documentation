# MySQL service fails starting up

It has been noticed that under certain circumstances, and when using a service type `mysql`, it can fail at the time of connecting to the service.

Let's assume we have the following code under the `.gitlab-ci.yml` file (example taken from <https://gitlab.com/gitlab-examples/mysql>):

```yaml
services:
- mysql

variables:
  # Configure mysql service (https://hub.docker.com/_/mysql/)
  MYSQL_DATABASE: hello_world_test
  MYSQL_ROOT_PASSWORD: mysql

connect:
  image: mysql
  script:
  - echo "SELECT 'OK';" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
```

If we run this job, we will obtain the following error:

```bash
...
Executing "step_script" stage of the job script
00:00
$ echo "SELECT 'OK';" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 2003 (HY000): Can't connect to MySQL server on 'mysql:3306' (111)
Cleaning up project directory and file based variables
00:01
ERROR: Job failed: command terminated with exit code 1
```

The error is produced because `mysql` service is a bit heavy, and it take some time to initialized. Containers is ready, but the service inside it is not.

This can be solved by delaying a bit our workflow, setting some `sleep`, as follows:

```yaml
connect:
  image: mysql
  script:
  - sleep 20 # sleep a certain amount of time so that we give enough time for mysql service to initialize.
  - echo "SELECT 'OK';" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
```

And that's it. This will now allow us to connect to the `mysql` service:

```bash
...
Executing "step_script" stage of the job script
00:20
$ sleep 20
$ echo "SELECT 'OK';" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
mysql: [Warning] Using a password on the command line interface can be insecure.
OK
OK
Cleaning up project directory and file based variables
00:01
Job succeeded
```

Further information can be found under the original upstream issue:

- [Can't connect to MySQL service with .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1552)

---

Not working? Please submit a ticket with us [through the Service Portal](https://cern.service-now.com/service-portal?id=service_element&name=git-service)
