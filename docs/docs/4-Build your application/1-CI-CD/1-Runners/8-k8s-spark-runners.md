---
title: Kubernetes+executor Spark Runners
---

A new fleet of runners have been deployed at CERN, centrally managed and using what is called the [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html), targeting Apache Spark on Hadoop workloads.

GitLab Spark runners are specialized runners that can execute CI/CD jobs on Apache Spark, popular component to run jobs on data stored in Hadoop.

These runners are meant to be used for common workflows such as data preparation, SQL reporting, etc., triggering jobs from the GitLab side and communicate with the Spark clusters.

## Usage

In order to start using the K8s ARM Runners, a user needs to add one the tags mentioned below to its `.gitlab-ci.yml` configuration file.

- `k8s-cvmfs-spark`

As an example, you will use:

```yaml
job:
  tags:
  - k8s-cvmfs-spark
  script:
  - echo "do something in Spark..."
```

For examples of CI job definitions using the Spark runners, see <https://hadoop-user-guide.web.cern.ch/spark/spark_gitlabci/>.

## General considerations for Kubernetes+executor ARM Runners

| Specs                 | Value                     |
| --------------------- | ------------------------- |
| Worker specs (2x)     | 16 vCPU - 29.3 GiB RAM    |
| Concurrent jobs (max) | 8                         |
| Default image         | HEP Workload Builder      |
| CVMFS available       | ✅                        |
| EOS available         | ✅                        |
| Privileged            | ❌                        |
| Runner configuration  | [K8s Spark Runners](https://gitlab.cern.ch/vcs/gitlab-runners/-/blob/master/chart/environments/k8s-spark-prod-v26.yaml) |

These K8s Spark Runners use the [`Kubernetes-executor`](https://docs.gitlab.com/runner/executors/kubernetes.html). This means that jobs executed by these runners will run on a dedicated Kubernetes cluster, aiming at improving scalability, resource optimization, isolation and portability.

Regarding the image, these runners use the **HEP Wprkload Builder** image by default (concretely `gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/hep-workload-builder`). Users are allowed to replace the image at their best convenience, as it was the case nowadays by setting the [`image`](https://docs.gitlab.com/ee/ci/yaml/#image) keyword appropriately.

An example of using an image other than the default one provided is:

```yaml
job:
  image: gitlab-registry.cern.ch/my-image
```

Support for accessing both `cvmfs` and `eos` is provided, since every job will have mounted a volume under the `/cvmfs` path and `/eos`, respectively.

These set of K8s Spark Runners will run as **`non-privileged`**, meaning that `docker` commands are unlikely to work.

These runners have the [Interactive Web Terminals](https://docs.gitlab.com/ee/ci/interactive_web_terminal/) enabled, allowing users to debug during the CI job runtime. As of April 2020, the web terminal is only available for the duration of the job: to debug why a specific command fails in the CI job script, it may be necessary to replace the failing command to troubleshoot with a `sleep` command and run the failing command interactively in the web terminal while the main job sleeps.

## Fair use

:::info

Please, remember that these runners are **shared among all users**, so kindly avoid massive pipelines and CI stages with more than 5 jobs in parallel or that run with a [parallel configuration](https://gitlab.cern.ch/help/ci/yaml/index.md#parallel) higher than 5.

:::

If you need to run these pipelines, please deploy your own private runners to avoid affecting the rest of the users.

See [Rate Limits](./99-rate-limits.md) for further information about limits set in the infrastructure.

---

## Feedback welcomed

We kindly ask you [to submit feedback](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=git-service) whether you encounter any issue on your workflows, this will allow us to keep improving our beloved infrastructure!
