---
title: Docker multi-arch Runners
---

:::info

Git Service is starting offering these Runners centrally. Check [K8s ARM Runners](./7-k8s-arm-runners.md) out.

:::

Our colleagues from [Linux](https://linux.web.cern.ch/support/) provide a pipeline to build multi-arch (currently `x86_64` and `aarch64`) Docker images, as well as dedicated runners for it. You can find instructions under the <https://gitlab.cern.ch/ci-tools/docker-builder> repository.

Please note that these runners are not supported via the official GitLab support channel, but there is a discussion channel about it under [AArch64/ARM64](https://mattermost.web.cern.ch/it-dep/channels/aarch64-arm64).
