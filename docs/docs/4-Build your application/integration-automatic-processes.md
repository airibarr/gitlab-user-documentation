---
title: Integrate GitLab with automatic processes
---

## How do I enable my GitLab project to integrate with automatic processes? (Koji, Jenkins, automatic deployment...)

GitLab application provides several mechanisms to make your code available to automatic processes such as package builders, continuous integration workflows or unattended deployment processes.

GitLab offers a number of dedicated mechanisms to integrate with specific tools (Jira, Jenkins...). These are listed in the **services** available for your project:

- From the project's main page, navigate `Settings` -> `Integrations` and scroll down to `Project services`.

See also [GitLab's help pages about integration](https://gitlab.cern.ch/help/integration/index.md).

## Koji (RPM builds)

For the specific case of building packages with the Koji build service, please check the dedicated [Koji documentation](https://linux.web.cern.ch/koji/).

## GitLab CI & Jenkins CI

[GitLab CI](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003690) is fully integrated with GitLab and does not require any further configuration.

When using GitLab with Jenkins CI, please refer to the [Jenkins documentation](https://jenkins.docs.cern.ch/revision-control-systems/git/gitlab/).

## General case

For the general case: your automatic build/CI/deployment system will need read access to your GitLab project.

The recommended way to achieve this is by using deployment credentials attached to the project rather than a particular user: GitLab Deploy keys (when using SSH to access the repository) or tokens (when using HTTPS to access the repository). Such deployment keys or tokens provide read-only access to a specific project.

## Deploy tokens (HTTPS)

See [GitLab's documentation about Deploy Tokens](https://gitlab.cern.ch/help/user/project/deploy_tokens/index.md).

## Deploy keys (SSH)

The Deploy Key provides read-only access to the repository via SSH. A project can have many Deploy Keys and a same Deploy Key can also be granted access to several projects. For each process that needs access to the repository, generate a new SSH key and upload the public part of the key as a Deploy Key to your project; the machine in possession of the private key can then clone/pull/fetch the GitLab repository. More details about Deploy Keys can be found in the [dedicated GitLab help pages](https://gitlab.cern.ch/help/user/ssh.md).

When using Deploy Keys, it is usually necessary to perform a git operation interactively once to accept the GitLab server's SSH key; for a fully unattended process, consider preemptively adding the GitLab server's SSH public keys to the SSH known_hosts file.

For Puppet-managed machines, simply include this class in your Puppet configuration:

```yaml
include cern_gitlab::gitlab_host_key
```

For other machines not managed by Puppet, add the following lines to the SSH `known_hosts` file (`~/.ssh/known_hosts` for individual users, or `/etc/ssh/ssh_known_hosts` for all users):

```bash
# ECDSA public key for gitlab.cern.ch.
# MD5 fingerprint: a7:4b:b5:e5:e6:a5:9d:35:6e:22:e0:6c:69:2d:70:bc
[gitlab.cern.ch]:7999 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBswtRsTLbTluK4lN2gk71Wp020NxNZcihjhUfUoJ7+hrHamzX5wjZBjwEBtgyzISYrstd1giRrP5qfqIf+dUyY=

# ED25519 public key for gitlab.cern.ch
# MD5 fingerprint: 7b:60:f1:4d:ef:ad:87:53:ab:93:35:82:3a:a6:07:b4
[gitlab.cern.ch]:7999 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINA6yUMtvqRusCLjveIEaFh+8o6ksnnIT7ckrhe8fiac

# RSA public key for gitlab.cern.ch.
# MD5 fingerprint: c6:1a:f0:bb:3b:09:39:37:15:1a:bf:3d:c9:99:4f:07
[gitlab.cern.ch]:7999 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubVS0czgKaPkC5cfB75CI3XjgWBv7pj2ILiZezVf8okkCAYr+bf4w6VrH2pyv3NRUW5Mm8U/3zoSNcrYIqhFFpz6R62/3xt8hMrPKJKstbt1lSIr8QhEyD24SJEKugMi560BWRPkTzXrrFgxz0X4vuKKytpvihRsccZ7F1JaX76UCWEJ3Xr2BFCEnnN6gj9nvFr4gvSMneunWVLGw2KcHwS1OJfnWBlp3fB0rYWSxZAoVjcjZjvv3hioEftaTapff2PkdQIX//N9Cc555FzdmMzixTvU5j/i+QvjxWVbEBNSKI6te6udC4fYUZMePs2QQnqw9mXUQtaQtw+HV7utuw==
```
