---
title: Using Git LFS on CERN GitLab
---

## Introduction

The base Git version control system is inefficient at handling large files such as firmware binaries and datasets. The general recommendation is to keep Git repositories (including their whole history) under 1GB to preserve performance. Fortunately, large and binary files can be stored using a special `git-lfs` extension so they do not consume space in the Git repository. Git-lfs should be used to store large files in Git repositories.

To enable management of large files, GitHub designed an extension to Git: [Git Large File Storage](https://git-lfs.github.com/) or Git LFS. Git LFS is enabled by default on your <https://gitlab.cern.ch> projects, and it is highly recommended to store all big files with this feature.

Please also read the [Git best practices](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003887) for general advice on working with a Git repository.

## Requirements and recommendations

:::note

FFI: <https://docs.gitlab.com/ee/topics/git/lfs/index.html>

:::

Git LFS is enabled by default on CERN GitLab projects. We recommend using [SSH](https://docs.gitlab.com/ee/user/ssh.html) or [PAT](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to work with Git repositories using Git LFS.

:::tip

`git-lfs` has no support for Kerberos, and using HTTPS URLs can result in many password prompts (althought this last is valid).

:::

Git LFS is installed on `lxplus8` (running CC8). For installation instructions on other systems, please refer to <https://github.com/git-lfs/git-lfs/wiki/Installation>

To reduce the amount of password prompts when using HTTPS, one can use a Git credentials cache. On `lxplus8`, since home directories are on network filesystems while a local folder is necessary, we suggest using the following (leveraging the local TMPDIR folder automatically set up for lxplus sessions):

```bash
git config --global credential.helper "cache --timeout=3600 --socket=$TMPDIR/git-credential-cache-socket"
```

## Removing large files from existing repo (a.k.a. "rewriting history")

GitLab provides [some recommendations using the BFG tool](https://docs.gitlab.com/ee/topics/git/lfs/migrate_to_git_lfs.html).

:::warning

Rewriting history is a risky operation. Make sure to keep a complete local backup of your repository before doing any change!

:::

We recommend following GitLab's documentation above. An alternative method exists: subcommand `git lfs migrate` can be used to remove large files previously added to an existing repository and move them to LFS. Please refer to the git-lfs-migrate command documentation for details. For instance:

```bash
git lfs migrate import --everything --include="*.bin,*.stp"
```

Note that you must force-push each branch whose history was modified. It will probably be necessary to temporarily [disable protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html) (which prevent force-pushing; in particular the master branch is protected by default). After the operation, make sure to re-protect branches that were previously protected.

Note about reclaiming space on the server side: after the history is rewritten and force-pushed, future git clone operations should transfer a lot less data. You may however still see a high value in the repository size reported by GitLab: this is because the disk space on the server side is not reclaimed immediately. This is however not an issue in general.

When following the [GitLab documentation using the BFG tool](https://docs.gitlab.com/ee/topics/git/lfs/migrate_to_git_lfs.html), the last step will specifically reclaim disk space on the server side. If that step was skipped or another method such as `git lfs migrate` was used, the disk space on the server side will be reclaimed eventually but this can take a significant amount of time. In particular, the following criteria must be met (but are not sufficient):

- At least 2 days have past (this is the value used by gitlab.cern.ch for `gc.pruneExpire`)
- There were enough push operations to trigger a [server-side GC operation](https://docs.gitlab.com/ee/administration/housekeeping.html#manual-housekeeping), a.k.a. _housekeeping_ (this happens automatically every 50 pushes on <https://gitlab.cern.ch> though).
