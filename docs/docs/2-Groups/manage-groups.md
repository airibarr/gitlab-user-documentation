---
title: Groups Management
---

Personal and tests projects can be created immediately in your personal workspace in GitLab: just click the 'New Project' button.

For team and production projects, it is necessary to create a workspace (called a 'Group' in GitLab) to host the new Git repository, if one does not exist yet for your team. If the same team later needs additional repositories, they can be created in the same Group/workspace and share the same permissions.

To create a GitLab Group, navigate to the ["Groups" page](https://gitlab.cern.ch/dashboard/groups) from [GitLab's dashboard](https://gitlab.cern.ch/) and click the ["New Group"](https://gitlab.cern.ch/groups/new) button.

As the creator, you will be assigned the `Owner` role in the new group. You can find a detailed description of the permissions assign to the different GitLab roles in the [Permissions Help page](https://gitlab.cern.ch/help/user/permissions.md). At this point, it is a good idea to map the new Group to an e-group in order to grant your team access to the new Group:

1. For the new group's page (`https://gitlab.cern.ch/groups/_mynewgroup_`), navigate to `Settings` -> `LDAP Synchronization`.
2. In `Sync method`, select `LDAP Group cn`, and enter the e-group's name. (e.g., `web-services`).
    - **Only e-groups are accepted**, not e-group aliases.
3. In `LDAP Access`, select the [permissions level](https://gitlab.cern.ch/help/permissions/permissions.md) to apply to the e-group. It's also worth checking [permissions and roles](https://docs.gitlab.com/ee/user/permissions.html) from upstream documentation to identify what roles fit better to the different groups.

It is possible and **highly recommended** to configure multiple e-groups for the different permission levels. A user member belonging to several e-groups is granted the highest access level.

A few important notes about e-group mapping:

- **Configuring e-group synchronization will remove any existing member who is not also member of the e-group** (or member of at least one of the e-groups if multiple e-groups are configured). When configuring e-group synchronization, make sure you are member of at least one of the configured e-groups.
- E-group synchronization grants a minimum access level to all projects in the group. Each project in the group can be assigned additional members, also allowing to grant a higher access level (up to Owner) to some users in certain projects. This can be done by adding the desired user with the needed role in the member's page of the project.
- **Only users already known to GitLab are synchronized**. In other words, only users who have connected at least once to GitLab will be imported by the e-group mapping.
- There may be a delay of **up to one hour** after a user has been added to an e-group before access is granted to the GitLab workspaces mapped to that e-group. However, a **sync operation can be forced** by clicking in "Sync now" in the member page of the group (<https://gitlab.cern.ch/groups/_group_name_/group_members>)
- E-groups can only be mapped at the Group level. Projects will inherit permissions from their parent Group.

:::warning

**We strongly recommend** users not to increase the access level of a member of a sync'ed e-group from the Group tabs. Instead, you should follow the convention of creating proper e-groups with the appropriate permissions and letting LDAP synchronisation handle it. In the event of a full synchronization of e-groups, this could lead into a missing permissions situation if they are not set in the proper way (see incident happened in July 2022 [OTG0072212](https://cern.service-now.com/service-portal?id=outage&n=OTG0072212)), preventing users to access repositories.

:::

For further information about how to handle Groups in GitLab, please check out the official documentation at <https://docs.gitlab.com/ee/user/group/>
