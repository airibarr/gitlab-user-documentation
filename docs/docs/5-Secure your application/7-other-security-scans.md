---
title: Other Security Scans
---

In addition to source code analysis performed by SAST and Secret Detection, GitLab Ultimate offers several other scanning tools which ensure that an application is secure in the various stages of development. It is highly recommended that these scans be integrated into all project workflows.

## Analysis of running web applications

1. **[DAST (Dynamic Application Security Testing)](https://docs.gitlab.com/ee/user/application_security/dast/index.html)**: If you deploy your web application into a new environment, your application may become exposed to new types of attacks. For example, misconfigurations of your application server or incorrect assumptions about security controls may not be visible from the source code. DAST examines applications for vulnerabilities like these in deployed environments.
2. **[API Security](https://docs.gitlab.com/ee/user/application_security/dast_api/index.html)**: The DAST API analyzer for web APIs helps discover bugs and potential security issues that other QA processes may miss. The analyzer can test API types such as REST, SOAP and GraphQL; and content types such as Form bodies, JSON, and XML.
3. **[API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html)**: Web API fuzzing performs fuzz testing of API operation parameters. Fuzz testing sets operation parameters to unexpected values to cause unexpected behavior and errors in the API backend. This helps you discover bugs and potential security issues that other QA processes may miss.

## Dependency Analysis

1. **[Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html)**: Your application’s Docker image may itself be based on Docker images that contain known vulnerabilities. By including an extra Container Scanning job in your pipeline that scans for those vulnerabilities and displays them in a merge request, you can use GitLab to audit your Docker-based apps.

## Infrastructure Analysis

1. **[Infrastructure as Code Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html)**: IaC Scanning scans your IaC configuration files for known vulnerabilities. It supports configuration files for Terraform, Ansible, AWS CloudFormation, and Kubernetes.

## Enabling Scans: Quickstart

### Configure individual scans

To enable and configure these scans in your project:

1. On the left sidebar of your project, select **Security and Compliance > Security Configuration**.
2. On the tab bar, select **Security Testing**.
3. Enable the scan of your choice.

![Enabling Scans](/img/assets/secure-your-application/other-security-scans/enabling_other_scans.png)

### Enable AutoDevOps

To enable all these scans, developers can use [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html) which includes the following [stages](https://docs.gitlab.com/ee/topics/autodevops/stages.html).

![Enabling Scans](/img/assets/secure-your-application/other-security-scans/autodevops_stages.png)

More instructions on how to enable AutoDevOps can be found [here](https://docs.gitlab.com/ee/topics/autodevops/index.html#enable-or-disable-auto-devops). 


## Reference

1. GitLab documentation page on *Application Security* - [Application security | Gitlab](https://docs.gitlab.com/ee/user/application_security/)
