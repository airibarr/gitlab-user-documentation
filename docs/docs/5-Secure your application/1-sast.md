---
title: SAST (Static Application Security Testing)
---

GitLab's *SAST* (Static Application Security Testing) analyzers can be used to check source code for known vulnerabilities. The analyzers output JSON-formatted reports as job artifacts. The results are sorted by the priority of the vulnerability - Critical, High, Medium, Low, Info, Unknown. GitLab *SAST* supports scanning a variety of programming languages and frameworks. Once *SAST* is enabled, the right set of analyzers runs automatically even if your project uses more than one language. The full list of supported languages and frameworks can be found [here](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks). 

## How to enable it

:::info

Pre-requisite: GitLab CI/CD configuration (```.gitlab-ci.yml```) must include the `test` stage.

:::

To enable SAST, you can use one of the following methods:

1. Use an automatically configured merge request. This method prepares a merge request, with the SAST template included in the ```.gitlab-ci.yml``` file. This can be set up under **Secure > Security configuration**, in the **Static Application Security Testing (SAST)** row. Select **Enable SAST** and review and merge the changes.

    ![Configure SAST with an MR](/img/assets/secure-your-application/sast/configure-mr.png)

2. Manually [include](https://docs.gitlab.com/ee/ci/yaml/index.html#includetemplate) the ```SAST.gitlab-ci.yml``` [template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml) in your ```.gitlab-ci.yml```. This method can be used if your pipeline is complex. 
    
    a. On the top bar, select Main menu > Projects and find your project.
    
    b. On the left sidebar, select CI/CD > Editor.
    
    c. Copy and paste the following to the bottom of the ```.gitlab-ci.yml``` file -
    
    ```yaml
    stages:
      - <another-stage-if-needed>
      - test

    include:
      - template: Jobs/SAST.gitlab-ci.yml
    ```
    
    d. Validate the pipeline and commit these changes. Pipelines now include a SAST job.
  
The supplied template generates *<analyzer_name>_sast* jobs for your CI/CD pipeline and checks the source code of your project for potential security flaws. In each case, the scan results can be found in the **Security and Compliance > Vulnerability Report** tab. 


## Reference

1. GitLab documentation page on *SAST* - [Static Application Security Testing (SAST) | Gitlab](https://docs.gitlab.com/ee/user/application_security/sast/)
