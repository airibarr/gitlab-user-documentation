---
title: Security Training
---

GitLab offers security training to help developers learn how to fix vulnerabilities. This training is provided by the following third-party vendors:
1. [Kontra](https://application.security/)
2. [Secure Code Warrior](https://www.securecodewarrior.com/) 

:::info

Security training is available for free for the version of Gitlab that CERN currently provides (Ultimate). Some third-party training vendors may require you to sign up for a free account. GitLab does not send any user information to these third-party vendors. It does send the CWE or OWASP identifier and the language name of the file extension.

:::

To enable security training for vulnerabilities in your project:

1. On the left sidebar of your project, select **Security and Compliance > Security Configuration**.
2. On the tab bar, select **Vulnerability Management**.
3. To enable a security training provider, turn on the toggle.

![Enable Security Training](/img/assets/secure-your-application/security-training/enable_security_training.png)

The vulnerability page now includes a training link relevant to the detected vulnerability. The availability of training depends on whether the enabled training vendor has content matching the particular vulnerability. Therefore, some vulnerabilities may not display training content. Vulnerabilities with a CWE are more likely to return a training result.

![Security Training](/img/assets/secure-your-application/security-training/security_training.png)


## Reference

1. GitLab documentation page on *Enable security training for vulnerabilities* - [Vulnerability Page | Gitlab](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#enable-security-training-for-vulnerabilities)
