---
title: Secret Detection
---

You can use GitLab *Secret Detection* to search your repositories for secrets in order to lessen the likelihood of secrets being added to a Git repository. Although scanning is independent of language and framework, it does not support scanning binary files.
To search the repository for secrets, *Secret Detection* employs an analyzer that includes the Gitleaks tool. Detection occurs in the ```secret_detection``` job. A *Secret Detection* report artifact is saved with the findings, which you can download and review at a later time.

## How to enable it

:::info

Pre-requisite: GitLab CI/CD configuration (```.gitlab-ci.yml```) must include the `test` stage.

:::

To enable Secret Detection, you can use one of the following methods:

1. Use an automatically configured merge request. This method prepares a merge request, with the Secret Detection template included in the `.gitlab-ci.yml file`. This can be set up under **Secure > Security configuration**, in the **Secret Detection** row. Select **Configure with a merge request** and review and merge the changes.

    ![Configure Secret Detection with an MR](/img/assets/secure-your-application/secret-detection/configure-mr.png)

2. Edit the ```.gitlab.ci.yml``` file manually. This method can be used if your ```.gitlab-ci.yml``` file is complex.
    
    a. On the top bar, select Main menu > Projects and find your project.
    
    b. On the left sidebar, select CI/CD > Editor.
    
    c. Copy and paste the following to the bottom of the ```.gitlab-ci.yml``` file -
    
    ```yaml
    stages:
      - <another-stage-if-needed>
      - test
      
    include:
      - template: Jobs/Secret-Detection.gitlab-ci.yml
    ```
    
    d. Validate the pipeline and commit these changes. Pipelines now include a Secret Detection job.

The supplied template generates a *secret_detection* job for your CI/CD pipeline and checks the source code for any leaked credentials. In each case, the scan results can be found in the **Security and Compliance > Vulnerability Report** tab. 


## Reference

1. GitLab documentation page on secret detection - [Secret Detection | Gitlab](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
2. GitLab documentation page on Auto DevOps - [Auto DevOps | GitLab](https://docs.gitlab.com/ee/topics/autodevops/index.html) 
