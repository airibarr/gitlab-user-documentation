---
title: Scan Results
---

## Security Dashboard

GitLab (Ultimate) provides users with a Security Dashboard, where they can view a summary of the results of various scans. This can be accessed via the **Security and Compliance > Security dashboard** tab. 

![Security Dashboard](/img/assets/secure-your-application/scan-results/security_dashboard.png) 

## Vulnerability Report

A comprehensive list of the vulnerabilities found in successful scans can be accessed via the **Security and Compliance > Vulnerability Report** tab.

![Vulnerability Report](/img/assets/secure-your-application/scan-results/vulnerability_report.png)

Each listed vulnerability points to its location in code, the [CWE](https://cwe.mitre.org/data/index.html) or [CVE](https://cve.mitre.org/cve/), which scanner was responsible for detecting it and other relevant details. 

![Vulnerability Details](/img/assets/secure-your-application/scan-results/vulnerability_details.png)


## Reference

1. GitLab documentation page on *Security Dashboard* - [GitLab Security Dashboards and Security Center | Gitlab](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
2. GitLab documentation page on *Vulnerability Report* - [Vulnerability Report | Gitlab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)
3. GitLab documentation page on *Vulnerability Page* - [Vulnerability Page | Gitlab](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/)
