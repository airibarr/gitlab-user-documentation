---
title: Secure your application
---

Software and services face increased risk due to suboptimal/insecure coding practices and exposed/public secrets in version-controlled projects. GitLab tools can analyze source code, dependencies, vulnerabilities in a running application and infrastructure as code configuration. Each of the GitLab application security tools is relevant to specific stages of the feature development workflow.

![Tool and CICD Stages](/img/assets/secure-your-application/index/tools_and_cicd_stages.png)

The *Static Application Security Testing (SAST)* and *Secret Detection* tools, which detect the presence of secrets and vulnerabilities in your code, are a good starting point for the inclusion of security scans in your project. The following sections provide instructions on how to enable and use these tools on GitLab, to keep your code secure.
