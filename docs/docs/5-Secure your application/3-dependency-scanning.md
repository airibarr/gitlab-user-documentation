---
title: Dependency Scanning
---

GitLab's Dependency Scanning analyzes your application’s dependencies for known vulnerabilities. All dependencies are scanned, including transitive dependencies, also known as nested dependencies. GitLab analyzers obtain dependency information using one of the following two methods:

1. [Parsing lockfiles directly](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#obtaining-dependency-information-by-parsing-lockfiles)
2. [Running a package manager or build tool to generate a dependency information file which is then parsed](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#obtaining-dependency-information-by-running-a-package-manager-to-generate-a-parsable-file)

Dependency Scanning automatically detects the languages used in the repository. All analyzers matching the detected languages are run. There is usually no need to customize the selection of analyzers. However, CI/CD variables can be used to [customize analyzer behaviour](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-analyzer-behavior). The full list of supported languages and package managers can be found [here](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers). 

## How to enable it

:::info

Pre-requisite: GitLab CI/CD configuration (```.gitlab-ci.yml```) must include the `test` stage.

:::

To enable Dependency Scanning, you can use one of the following methods:

1. Use an automatically configured merge request. This method prepares a merge request, with the Dependency Scanning template included in the ```.gitlab-ci.yml``` file. This can be set up under **Secure > Security configuration**, in the **Dependency Scanning** row. Select **Configure with a merge request** and review and merge the changes.

    ![Configure Dependency Scanning with an MR](/img/assets/secure-your-application/dependency-scanning/configure-mr.png)

2. Manually [include](https://docs.gitlab.com/ee/ci/yaml/index.html#includetemplate) the ```Dependency-Scanning.gitlab-ci.yml``` [template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Dependency-Scanning.gitlab-ci.yml) in your ```.gitlab-ci.yml```. This method can be used if your pipeline is complex.
    
    a. On the top bar, select Main menu > Projects and find your project.

    b. On the left sidebar, select CI/CD > Editor.

    c. Copy and paste the following to the bottom of the ```.gitlab-ci.yml``` file -
    
      ```yaml
      stages:
        - <another-stage-if-needed>
        - test

      include:
        - template: Security/Dependency-Scanning.gitlab-ci.yml
      ```
    
    d. Validate the pipeline and commit these changes. Pipelines now include a Dependency Scanning job.

The supplied template generates *<analyzer_name>_dependency_scanning* jobs for your CI/CD pipeline and checks project dependencies for vulnerable package versions. In each case, the scan results can be found in the **Secure > Vulnerability Report** tab. 

## Dependency List

This scan also generates the *Dependency List* for a project which is a collection of dependencies in your project, including existing and new findings. This information is sometimes referred to as a Software Bill of Materials, SBOM, or BOM. This can be accessed from the **Secure > Dependency List** tab as shown below.

![Dependency List](/img/assets/secure-your-application/dependency-scanning/dependency-list.png)


## Reference

1. GitLab documentation page on *Dependency Scanning* - [Dependency Scanning | Gitlab](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
