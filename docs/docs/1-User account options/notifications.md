---
title: Mail notifications on code push in GitLab
---

This article covers the specific case of email notification whenever code is pushed to a GitLab repository, and how to make it work with CERN email and e-groups. For general notification on activity in GitLab (Issues, Merge Requests, discussions etc.) please refer to the [GitLab notification documentations](https://docs.gitlab.com/ee/user/profile/notifications.html)

GitLab provides an option to send e-mail notifications whenever commits are pushed to a repository. From a project's page, navigate to "Settings", then "Integration", find "Project services", open "Emails on Push" and check the "Active" box. Multiple recipients can be specified, separated by white space.

When sending notifications to e-groups with posting restrictions, make sure to disable the option `send from committer`: users can specify any email address they want in a git commit's author information, and this email address may not match the email address expected in the posting restrictions, resulting in notifications not being delivered.

With "send from committer" disabled, GitLab will consistently send notifications using sender <noreply-gitlab@cern.ch>. If necessary, add e-group "no-reply" to the posting restrictions of the e-group receiving the notifications in order to make sure it accepts the notifications sent by GitLab. The notification messages will still show the developer name in the "From" display name, so it is still clear who pushed the commit even with the "send from committer" option disabled.

## Filter notifications in your mail client

To filter notifications received from GitLab, it is recommended for users to **filter by GitLab-specific headers**, instead of by sender email. Check out the upstream documentation [Email headers you can use to filter email](https://docs.gitlab.com/ee/user/profile/notifications.html#email-headers-you-can-use-to-filter-email) about the available headers.
Thus, your filter will survive in the event of a future change in the sender email address, as it happened in March 2024 (ffi: [OTG0148869](https://cern.service-now.com/service-portal?id=outage&n=OTG0148869)).

As an example, users can filter emails by setting a rule for the `X-GitLab-Project:` GitLab-specific header, as shown below:

![Create rule based on email header](/img/assets/user-account-options/set-filter-rules-with-headers.png)

For further information about other type of filters you may want to apply in your mailbox, have a look to the official documentation provided by Microsoft about [setting up rules](https://support.microsoft.com/en-us/office/set-up-rules-in-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41).

It is also worth mentioning the recommendations given by our colleagues from the E-mail Service, that can be found in the [CERN E-Mail Service User Guide - FAQ](https://mailservices.docs.cern.ch/ExchangeOnline/faq/).

## Searching for notifications in your mail client

To search for the GitLab nofitications obtained in your mail client, there are several factors that may determine the result of your search. For example, depending on your mail client, you can use different ways of searching for a specific words.

For example, some clients let you search by sender, like `sent: noreply-gitlab@cern.ch`, and this will uncover all messages sent by the GitLab notifications `noreply-gitlab@cern.ch` sender address email.

For more information about the topic, refer to the following official documentations:

- [How to search in Outlook](https://support.microsoft.com/en-us/office/how-to-search-in-outlook-d824d1e9-a255-4c8a-8553-276fb895a8da)
- [How to search in Gmail](https://support.google.com/mail/answer/6593?hl=en&co=GENIE.Platform%3DDesktop)
- [How to search in Mozilla Thunderbird](https://support.mozilla.org/en-US/kb/global-search)
