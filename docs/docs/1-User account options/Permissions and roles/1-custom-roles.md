---
title: Custom roles and permissions
---

Group admins have the ability to create and assign custom roles for members in their group. Those custom roles are useful in case you want to enhance a user's permissions without giving them a higher role. This is useful when a user with a specific role is needed to perform actions that are restricted for his/hers pre-defined role.

## How to create a custom role?

1. Go to your group **Settings** and check the dropdown menu
2. Click on **Roles and Permissions** to open its page
3. On the right side of the table there is a button named **Add new role**, click it and set the following:
    1. **Base role**
    2. **Name** of the custom role
    3. **Permissions** you want to add for the specific role.

## Roles and Permissions page returns a 404 error page

Currently, there is a bug regarding the **Roles and Permissions** page when the group is synced via LDAP.
Opening this page will result in rendering a 404 page and you cannot create a custom role.

Fortunately, the gitlab admins can assist you on creating your custom role via the admin panel.

To do this please [open a ticket](https://cern.service-now.com/service-portal?id=service_element&name=git-service) with us and:

1. Specify the group you want to add a custom role.
2. Provide the preferred **Name** of the custom role you want to name.
3. Choose the **Base Role** from the [role list](https://docs.gitlab.com/ee/user/permissions.html#roles)
4. Choose the **Permissions** you want to add from this [abilities list](https://docs.gitlab.com/ee/user/custom_roles/abilities.html)

Once you provide the above information we will handle the creation of the custom role.

:::info

It is not possible to assign a custom role to a LDAP synchronization rule but you can assign it to users and groups in the sub-containing projects

:::
