# Access to GitLab at CERN

The GitLab service **only supports CERN accounts** (this **does not include lightweight/external accounts**). It is thus not possible to share a GitLab project with users outside CERN, who do not have a CERN account, without making the project Public.

Whether your project has contributors who are not affilated with CERN, you can opt for:

- To host your code on a different web-based Git repository as per [Appropriate usage of GitLab](../0-getting-started.md#appropriate-usage-of-gitlab) instructions.
- To refer to your secretariat to obtain a CERN computing account for those **known** external collaborators as part of the `EXTN/DIST` status.

:::info

Note that when asking for a `EXTN/DIST` status account, this must be done for people/collaborators who you know and trust. This is a full-fledged CERN computing account so it should be used with responsibility.

:::
