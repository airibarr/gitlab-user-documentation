---
title: Authentication
---

## Kerberos authentication

Kerberos authentication is available for Git operations against repositories hosted in GitLab and is the recommended authentication method from CERN computers. Exceptions are when working from outside the CERN network or there is a need to store large binary files with [Git LFS](https://cern.service-now.com/service-portal?id=kb_article&n=KB0005988), in which case SSH is a better option (see next section).

:::info

Before Kerberos can be used, make sure to visit <https://gitlab.cern.ch> in a web browser at least once to initialize your account on GitLab.

:::

It uses a specific Git remote URL, which can be found in the project's main page (<https://gitlab.cern.ch/_namespace_name_/_project_name_>) under clone URL label "_KRB5_". Note that it remains necessary to sign in to <https://gitlab.cern.ch> with a web browser at least once to initialize your GitLab account before Git can be used.

Recent versions of Git (git > `2.11`) may require the following setting for Kerberos to work (ffi <https://docs.gitlab.com/ee/integration/kerberos.html#http-basic-access-denied-when-cloning>):

```bash
git config --global http.emptyAuth true
```

The above setting is typically necessary if seeing the error "remote: HTTP Basic: Access denied" when trying to use Kerberos authentication.

Kerberos authentication is available both inside and outside the CERN network as long as a valid CERN Kerberos ticket is available:

- Windows computers managed by CERN (i.e. joined to the CERN domain) can use Kerberos without further configuration while on the CERN network. Please make sure to [install Git for Windows using CMF](https://cmf.web.cern.ch/cmf/ComputerFramework/AddRemove.aspx) as not all versions available from the Git web site support Kerberos.
- `lxplus8` and CERN-managed Linux computers typically have Kerberos authentication configured. For more information about configuring and using Kerberos on Linux please see <https://linux.web.cern.ch/docs/kerberos-access/>
- The [Linux documentation](https://linux.web.cern.ch/docs/kerberos-access/) also applies to Mac OSX. In addition, Mac users may find information about using Kerberos in the [support-macosx egroup archives](https://groups.cern.ch/group/support-macosx/Lists/Archive/Flat.aspx?RootFolder=/group/support-macosx/Lists/Archive/LXPLUS%20SSH%20Authentication%20via%20Kerberos%20Token%20using%20Mac%20OS%20X%2010.9&FolderCTID=0x0120020096447BF68F7AC640B25ED65ECCD022FB). In case it still doesn't work, when using macports there may be issues with the version of the curl library: [KB0001849](https://cern.service-now.com/service-portal?id=kb_article&n=KB0001849).

## SSH access

GitLab offers repository access via SSH and authentication with SSH key. In order to use SSH to access repositories hosted on GitLab, please [map a SSH public key](https://resources.web.cern.ch/resources/Manage/Accounts/MapSSH.aspx) to your CERN account. More information about SSH keys in [KB0003136](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003136).

Note that GitLab also supports the HTTPS protocol with CERN username/password authentication, which is sufficient for casual access.

Further information can be fetched from the official documentation at <https://docs.gitlab.com/ee/user/ssh.html>

## Two-Factor authentication (2FA Authentication)

GitLab allows users to enable Two-Factor authentication (2FA Authentication).

The 2FA implementation applies to the GitLab web interface and Git access over HTTP. However, Git access over SSH or Kerberos, will not require the second level of protection.

Further information can be fetched from the official documentation at <https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html>

## Authenticating with the GitLab API

Refer to the official documentation under <https://gitlab.cern.ch/help/api/index.md#authentication> for authenticating with the GitLab API.
