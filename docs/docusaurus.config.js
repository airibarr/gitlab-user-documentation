/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'GitLab at CERN',
  tagline: 'Get hands-on experience with GitLab',
  url: 'https://gitlab.docs.cern.ch',
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/apple-touch-icon.png',
  organizationName: 'GitLab',
  projectName: 'vcs/gitlab-user-documentation', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Welcome to GitLab at CERN',
      logo: {
        alt: 'GitLab Logo',
        src: 'img/gitlab-logo-500.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.cern.ch/vcs/gitlab-user-documentation',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'GitLab official documentation',
              href: 'https://docs.gitlab.com/',
            },
            {
              label: 'GitLab Forum',
              href: 'https://forum.gitlab.com/',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/gitlab',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              href: 'https://about.gitlab.com/blog/',
            },
            {
              label: 'About GitLab',
              href: 'https://about.gitlab.com/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} CERN`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: ({docPath}) => `https://gitlab.cern.ch/vcs/gitlab-user-documentation/edit/master/docs/docs/${docPath}`,
          path: 'docs',
          breadcrumbs: true,
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,  
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  // from https://github.com/easyops-cn/docusaurus-search-local
  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        highlightSearchTermsOnTargetPage: true,
        // For Docs using Chinese, The `language` is recommended to set to:
        // ```
        // language: ["en", "zh"],
        // ```
      },
    ],
  ],
};
