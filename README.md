# GitLab User Documentation

User facing documentation intended for internal user of the personnel at CERN.

This documentation is deployed under <https://gitlab.docs.cern.ch>

## Develop this documentation

To develop this documentation, we can:

```bash
docker run -it --rm -p 3000:3000 --entrypoint=/bin/bash -v "C:\path\to\gitlab-user-documentation\:/documentation" -w "/documentation" registry.cern.ch/docker.io/library/node:16
root@xxx/documentation: cd docs
root@xxx:/documentation/docs# yarn start -h 0.0.0.0

# If new version of Docusaurus, consider upgrading with (follow server instructions):
# yarn upgrade @docusaurus/core@latest @docusaurus/preset-classic@latest
```
